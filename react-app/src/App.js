import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import logo from "./logo.svg";
// import "./App.css";
import "./assets/css/style.css";
import "./assets/css/plugins.css";
import "./assets/css/ionicons.min.css";
import "./assets/css/simple-line-icons.css";
import "./assets/css/bootstrap.min.css";
// import "bootstrap/dist/css/bootstrap.min.css";
import Navbar from "./components/Navbar";
import ProductList from "./components/productlist";
import Product from "./components/Product";
import Details from "./components/Details";
import Cart from "./components/Cart";
import Login from "./components/Login";
import Signup from "./components/Signup";
import Blog from "./components/Blog";
import ContactUs from "./components/ContactUs";
import PageNotFound from "./components/PageNotFound";
import About from "./components/About";
class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Navbar />
        <Switch>
          <Route path="/details" component={Details} />
          <Route path="/cart" component={Cart} />
          <Route path="/about" component={About} />
          <Route path="/login" component={Login} />
          <Route path="/signup" component={Signup} />
          <Route path="/shop" component={Product} />
          <Route path="/blog" component={Blog} />
          <Route path="/contact" component={ContactUs} />
          <Route path="/" component={ProductList} />
          <Route component={PageNotFound} />
        </Switch>
      </React.Fragment>
    );
  }
}

export default App;
