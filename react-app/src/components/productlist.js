import React, { Component } from "react";
import Product from "./Product";
import { Link } from "react-router-dom";
export default class ProductList extends Component {
  render() {
    return (
      <div>
        <div
          className="single-slide slick-slide slick-current slick-active"
          style={{
            backgroundImage: 'url("../assets/images/slider/slider-bg-3.jpg")',
            width: 1838,
            position: "relative",
            left: 0,
            top: 0,
            zIndex: 999,
            opacity: 1,
          }}
          data-slick-index={0}
          aria-hidden="false"
          tabIndex={0}
        >
          {/* Hero Content One Start */}
          <div className="hero-content-one container">
            <div className="row">
              <div className="col-lg-6 col-md-8">
                <div className="slider-text-info pt-0">
                  <h3 className>Top Selling!</h3>
                  <h1>New Furniture</h1>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua.
                  </p>
                  <a
                    href="shop.html"
                    className="btn slider-btn uppercase"
                    tabIndex={0}
                  >
                    <span>SHOP NOW</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
          {/* Hero Content One End */}
        </div>
        <div className="banner-area section-ptb">
          <div className="container">
            <div className="row">
              <div className="col-lg-6 col-md-6">
                {/* single-banner start */}
                <div className="single-banner">
                  <div className="banner-bg">
                    <Link to="/">
                      <img
                        src={require("../assets/images/banner/1.jpg")}
                        alt="image"
                      />
                    </Link>
                  </div>
                  <div className="banner-contet">
                    <p>30% Off</p>
                    <h3>Chair Collection </h3>
                    <a href="#" className="btn-3">
                      SHOP NOW
                    </a>
                  </div>
                </div>
                {/* single-banner end */}
              </div>
              <div className="col-lg-6  col-md-6">
                {/* single-banner start */}
                <div className="single-banner s-mt-30">
                  <div className="banner-bg">
                    <Link to="/">
                      <img
                        src={require("../assets/images/banner/2.jpg")}
                        alt="image"
                      />
                    </Link>
                  </div>
                  <div className="banner-contet">
                    <p>30% Off</p>
                    <h3>Chair Collection </h3>
                    <a href="#" className="btn-3">
                      SHOP NOW
                    </a>
                  </div>
                </div>
                {/* single-banner end */}
              </div>
            </div>
          </div>
        </div>
        <div className="product-area section-pb">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                {/* section-title start */}
                <div className="section-title">
                  <h2>New Arrivals</h2>
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit</p>
                </div>
                {/* section-title end */}
              </div>
            </div>
            {/* product-warpper start */}
            <div className="product-warpper">
              <div className="row">
                <div className="col-lg-3 col-md-4 col-sm-6">
                  {/* single-product-wrap start */}
                  <div className="single-product-wrap">
                    <div className="product-image">
                      <Link to="/details">
                        <img
                          src={require("../assets/images/product/1.jpg")}
                          alt=""
                        />
                      </Link>
                      <div className="product-action">
                        <a href="#" className="wishlist">
                          <i className="icon-heart" />
                        </a>
                        <a href="#" className="add-to-cart">
                          <i className="icon-handbag" />
                        </a>
                        <a
                          href="#"
                          className="quick-view"
                          data-toggle="modal"
                          data-target="#exampleModalCenter"
                        >
                          <i className="icon-shuffle" />
                        </a>
                      </div>
                    </div>
                    <div className="product-content">
                      <h3>
                        <Link to="/details">Products Name 001</Link>
                      </h3>
                      <div className="price-box">
                        <span className="old-price">140.00</span>
                        <span className="new-price">120.00</span>
                      </div>
                    </div>
                  </div>
                  {/* single-product-wrap end */}
                </div>
                <div className="col-lg-3 col-md-4 col-sm-6">
                  {/* single-product-wrap start */}
                  <div className="single-product-wrap">
                    <div className="product-image">
                      <Link to="/details">
                        <img
                          src={require("../assets/images/product/2.jpg")}
                          alt=""
                        />
                      </Link>
                      <div className="product-action">
                        <a href="#" className="wishlist">
                          <i className="icon-heart" />
                        </a>
                        <a href="#" className="add-to-cart">
                          <i className="icon-handbag" />
                        </a>
                        <a
                          href="#"
                          className="quick-view"
                          data-toggle="modal"
                          data-target="#exampleModalCenter"
                        >
                          <i className="icon-shuffle" />
                        </a>
                      </div>
                    </div>
                    <div className="product-content">
                      <h3>
                        <Link to="/details">Products Name 002</Link>
                      </h3>
                      <div className="price-box">
                        <span className="new-price">120.00</span>
                      </div>
                    </div>
                  </div>
                  {/* single-product-wrap end */}
                </div>
                <div className="col-lg-3 col-md-4 col-sm-6">
                  {/* single-product-wrap start */}
                  <div className="single-product-wrap">
                    <div className="product-image">
                      <Link to="/details">
                        <img
                          src={require("../assets/images/product/3.jpg")}
                          alt=""
                        />
                      </Link>
                      <div className="product-action">
                        <a href="#" className="wishlist">
                          <i className="icon-heart" />
                        </a>
                        <a href="#" className="add-to-cart">
                          <i className="icon-handbag" />
                        </a>
                        <a
                          href="#"
                          className="quick-view"
                          data-toggle="modal"
                          data-target="#exampleModalCenter"
                        >
                          <i className="icon-shuffle" />
                        </a>
                      </div>
                    </div>
                    <div className="product-content">
                      <h3>
                        <Link to="/details">Products Name 003</Link>
                      </h3>
                      <div className="price-box">
                        <span className="old-price">230.00</span>
                        <span className="new-price">210.00</span>
                      </div>
                    </div>
                  </div>
                  {/* single-product-wrap end */}
                </div>
                <div className="col-lg-3 col-md-4 col-sm-6">
                  {/* single-product-wrap start */}
                  <div className="single-product-wrap">
                    <div className="product-image">
                      <Link to="/details">
                        <img
                          src={require("../assets/images/product/4.jpg")}
                          alt=""
                        />
                      </Link>
                      <div className="product-action">
                        <a href="#" className="wishlist">
                          <i className="icon-heart" />
                        </a>
                        <a href="#" className="add-to-cart">
                          <i className="icon-handbag" />
                        </a>
                        <a
                          href="#"
                          className="quick-view"
                          data-toggle="modal"
                          data-target="#exampleModalCenter"
                        >
                          <i className="icon-shuffle" />
                        </a>
                      </div>
                    </div>
                    <div className="product-content">
                      <h3>
                        <Link to="/details">Products Name 004</Link>
                      </h3>
                      <div className="price-box">
                        <span className="new-price">120.00</span>
                      </div>
                    </div>
                  </div>
                  {/* single-product-wrap end */}
                </div>
                <div className="col-lg-3 col-md-4 col-sm-6">
                  {/* single-product-wrap start */}
                  <div className="single-product-wrap">
                    <div className="product-image">
                      <a href="#">
                        <img
                          src={require("../assets/images/product/5.jpg")}
                          alt=""
                        />
                      </a>
                      <div className="product-action">
                        <a href="#" className="wishlist">
                          <i className="icon-heart" />
                        </a>
                        <a href="#" className="add-to-cart">
                          <i className="icon-handbag" />
                        </a>
                        <a
                          href="#"
                          className="quick-view"
                          data-toggle="modal"
                          data-target="#exampleModalCenter"
                        >
                          <i className="icon-shuffle" />
                        </a>
                      </div>
                    </div>
                    <div className="product-content">
                      <h3>
                        <Link to="/details">Products Name 005</Link>
                      </h3>
                      <div className="price-box">
                        <span className="old-price">180.00</span>
                        <span className="new-price">150.00</span>
                      </div>
                    </div>
                  </div>
                  {/* single-product-wrap end */}
                </div>
                <div className="col-lg-3 col-md-4 col-sm-6">
                  {/* single-product-wrap start */}
                  <div className="single-product-wrap">
                    <div className="product-image">
                      <Link to="/details">
                        <img
                          src={require("../assets/images/product/6.jpg")}
                          alt=""
                        />
                      </Link>
                      <div className="product-action">
                        <a href="#" className="wishlist">
                          <i className="icon-heart" />
                        </a>
                        <a href="#" className="add-to-cart">
                          <i className="icon-handbag" />
                        </a>
                        <a
                          href="#"
                          className="quick-view"
                          data-toggle="modal"
                          data-target="#exampleModalCenter"
                        >
                          <i className="icon-shuffle" />
                        </a>
                      </div>
                    </div>
                    <div className="product-content">
                      <h3>
                        <Link to="/details">Products Name 006</Link>
                      </h3>
                      <div className="price-box">
                        <span className="new-price">130.00</span>
                      </div>
                    </div>
                  </div>
                  {/* single-product-wrap end */}
                </div>
                <div className="col-lg-3 col-md-4 col-sm-6">
                  {/* single-product-wrap start */}
                  <div className="single-product-wrap">
                    <div className="product-image">
                      <Link to="/details">
                        <img
                          src={require("../assets/images/product/7.jpg")}
                          alt=""
                        />
                      </Link>
                      <div className="product-action">
                        <a href="#" className="wishlist">
                          <i className="icon-heart" />
                        </a>
                        <a href="#" className="add-to-cart">
                          <i className="icon-handbag" />
                        </a>
                        <a
                          href="#"
                          className="quick-view"
                          data-toggle="modal"
                          data-target="#exampleModalCenter"
                        >
                          <i className="icon-shuffle" />
                        </a>
                      </div>
                    </div>
                    <div className="product-content">
                      <h3>
                        <Link to="/details">Products Name 007</Link>
                      </h3>
                      <div className="price-box">
                        <span className="old-price">250.00</span>
                        <span className="new-price">230.00</span>
                      </div>
                    </div>
                  </div>
                  {/* single-product-wrap end */}
                </div>
                <div className="col-lg-3 col-md-4 col-sm-6">
                  {/* single-product-wrap start */}
                  <div className="single-product-wrap">
                    <div className="product-image">
                      <Link to="/details">
                        <img
                          src={require("../assets/images/product/8.jpg")}
                          alt=""
                        />
                      </Link>
                      <div className="product-action">
                        <a href="#" className="wishlist">
                          <i className="icon-heart" />
                        </a>
                        <a href="#" className="add-to-cart">
                          <i className="icon-handbag" />
                        </a>
                        <a
                          href="#"
                          className="quick-view"
                          data-toggle="modal"
                          data-target="#exampleModalCenter"
                        >
                          <i className="icon-shuffle" />
                        </a>
                      </div>
                    </div>
                    <div className="product-content">
                      <h3>
                        <Link to="/details">Products Name 008</Link>
                      </h3>
                      <div className="price-box">
                        <span className="new-price">120.00</span>
                      </div>
                    </div>
                  </div>
                  {/* single-product-wrap end */}
                </div>
              </div>
            </div>
            {/* product-warpper start */}
          </div>
        </div>
        <div className="lg-banner-area lg-banner-bg-2 section-ptb">
          <div className="container">
            <div className="row">
              <div className="col-lg-7 offset-lg-5">
                <div className="lg-banner-info text-center mt--30 mb--30">
                  <h2>Contrary to popular belief is not simply rand.</h2>
                  <Link to="/shop" className="btn more-product-btn">
                    MORE PRODUCTS
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="product-area section-ptb">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                {/* section-title start */}
                <div className="section-title">
                  <h2>Featured Products</h2>
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit</p>
                </div>
                {/* section-title end */}
              </div>
            </div>
            {/* product-warpper start */}
            <div className="product-warpper">
              <div className="product-slider row slick-initialized slick-slider">
                <div className="slick-list draggable">
                  <div
                    className="slick-track"
                    style={{
                      opacity: 1,
                      width: 4800,
                      transform: "translate3d(-1200px, 0px, 0px)",
                    }}
                  >
                    <div
                      className="col slick-slide slick-cloned"
                      data-slick-index={-4}
                      aria-hidden="true"
                      style={{ width: 300 }}
                      tabIndex={-1}
                    >
                      {/* single-product-wrap start */}
                      <div className="single-product-wrap">
                        <div className="product-image">
                          <a href="product-details.html" tabIndex={-1}>
                            <img
                              src={require("../assets/images/product/11.jpg")}
                              alt=""
                            />
                          </a>
                          <div className="product-action">
                            <a href="#" className="wishlist" tabIndex={-1}>
                              <i className="icon-heart" />
                            </a>
                            <a href="#" className="add-to-cart" tabIndex={-1}>
                              <i className="icon-handbag" />
                            </a>
                            <a
                              href="#"
                              className="quick-view"
                              data-toggle="modal"
                              data-target="#exampleModalCenter"
                              tabIndex={-1}
                            >
                              <i className="icon-shuffle" />
                            </a>
                          </div>
                        </div>
                        <div className="product-content">
                          <h3>
                            <a href="product-details.html" tabIndex={-1}>
                              Products Name 005
                            </a>
                          </h3>
                          <div className="price-box">
                            <span className="old-price">130.00</span>
                            <span className="new-price">120.00</span>
                          </div>
                        </div>
                      </div>
                      {/* single-product-wrap end */}
                    </div>
                    <div
                      className="col slick-slide slick-cloned"
                      data-slick-index={-3}
                      aria-hidden="true"
                      style={{ width: 300 }}
                      tabIndex={-1}
                    >
                      {/* single-product-wrap start */}
                      <div className="single-product-wrap">
                        <div className="product-image">
                          <a href="product-details.html" tabIndex={-1}>
                            <img
                              src={require("../assets/images/product/12.jpg")}
                              alt=""
                            />
                          </a>
                          <div className="product-action">
                            <a href="#" className="wishlist" tabIndex={-1}>
                              <i className="icon-heart" />
                            </a>
                            <a href="#" className="add-to-cart" tabIndex={-1}>
                              <i className="icon-handbag" />
                            </a>
                            <a
                              href="#"
                              className="quick-view"
                              data-toggle="modal"
                              data-target="#exampleModalCenter"
                              tabIndex={-1}
                            >
                              <i className="icon-shuffle" />
                            </a>
                          </div>
                        </div>
                        <div className="product-content">
                          <h3>
                            <a href="product-details.html" tabIndex={-1}>
                              Products Name 012
                            </a>
                          </h3>
                          <div className="price-box">
                            <span className="new-price">120.00</span>
                          </div>
                        </div>
                      </div>
                      {/* single-product-wrap end */}
                    </div>
                    <div
                      className="col slick-slide slick-cloned"
                      data-slick-index={-2}
                      aria-hidden="true"
                      style={{ width: 300 }}
                      tabIndex={-1}
                    >
                      {/* single-product-wrap start */}
                      <div className="single-product-wrap">
                        <div className="product-image">
                          <a href="product-details.html" tabIndex={-1}>
                            <img
                              src={require("../assets/images/product/3.jpg")}
                              alt=""
                            />
                          </a>
                          <div className="product-action">
                            <a href="#" className="wishlist" tabIndex={-1}>
                              <i className="icon-heart" />
                            </a>
                            <a href="#" className="add-to-cart" tabIndex={-1}>
                              <i className="icon-handbag" />
                            </a>
                            <a
                              href="#"
                              className="quick-view"
                              data-toggle="modal"
                              data-target="#exampleModalCenter"
                              tabIndex={-1}
                            >
                              <i className="icon-shuffle" />
                            </a>
                          </div>
                        </div>
                        <div className="product-content">
                          <h3>
                            <a href="product-details.html" tabIndex={-1}>
                              Products Name 001
                            </a>
                          </h3>
                          <div className="price-box">
                            <span className="old-price">140.00</span>
                            <span className="new-price">120.00</span>
                          </div>
                        </div>
                      </div>
                      {/* single-product-wrap end */}
                    </div>
                    <div
                      className="col slick-slide slick-cloned"
                      data-slick-index={-1}
                      aria-hidden="true"
                      style={{ width: 300 }}
                      tabIndex={-1}
                    >
                      {/* single-product-wrap start */}
                      <div className="single-product-wrap">
                        <div className="product-image">
                          <a href="product-details.html" tabIndex={-1}>
                            <img
                              src={require("../assets/images/product/7.jpg")}
                              alt=""
                            />
                          </a>
                          <div className="product-action">
                            <a href="#" className="wishlist" tabIndex={-1}>
                              <i className="icon-heart" />
                            </a>
                            <a href="#" className="add-to-cart" tabIndex={-1}>
                              <i className="icon-handbag" />
                            </a>
                            <a
                              href="#"
                              className="quick-view"
                              data-toggle="modal"
                              data-target="#exampleModalCenter"
                              tabIndex={-1}
                            >
                              <i className="icon-shuffle" />
                            </a>
                          </div>
                        </div>
                        <div className="product-content">
                          <h3>
                            <a href="product-details.html" tabIndex={-1}>
                              Products Name 001
                            </a>
                          </h3>
                          <div className="price-box">
                            <span className="old-price">140.00</span>
                            <span className="new-price">120.00</span>
                          </div>
                        </div>
                      </div>
                      {/* single-product-wrap end */}
                    </div>
                    <div
                      className="col slick-slide slick-current slick-active"
                      data-slick-index={0}
                      aria-hidden="false"
                      style={{ width: 300 }}
                      tabIndex={0}
                    >
                      {/* single-product-wrap start */}
                      <div className="single-product-wrap">
                        <div className="product-image">
                          <a href="product-details.html" tabIndex={0}>
                            <img
                              src={require("../assets/images/product/9.jpg")}
                              alt=""
                            />
                          </a>
                          <div className="product-action">
                            <a href="#" className="wishlist" tabIndex={0}>
                              <i className="icon-heart" />
                            </a>
                            <a href="#" className="add-to-cart" tabIndex={0}>
                              <i className="icon-handbag" />
                            </a>
                            <a
                              href="#"
                              className="quick-view"
                              data-toggle="modal"
                              data-target="#exampleModalCenter"
                              tabIndex={0}
                            >
                              <i className="icon-shuffle" />
                            </a>
                          </div>
                        </div>
                        <div className="product-content">
                          <h3>
                            <a href="product-details.html" tabIndex={0}>
                              Products Name 009
                            </a>
                          </h3>
                          <div className="price-box">
                            <span className="old-price">150.00</span>
                            <span className="new-price">125.00</span>
                          </div>
                        </div>
                      </div>
                      {/* single-product-wrap end */}
                    </div>
                    <div
                      className="col slick-slide slick-active"
                      data-slick-index={1}
                      aria-hidden="false"
                      style={{ width: 300 }}
                      tabIndex={0}
                    >
                      {/* single-product-wrap start */}
                      <div className="single-product-wrap">
                        <div className="product-image">
                          <a href="product-details.html" tabIndex={0}>
                            <img
                              src={require("../assets/images/product/10.jpg")}
                              alt=""
                            />
                          </a>
                          <div className="product-action">
                            <a href="#" className="wishlist" tabIndex={0}>
                              <i className="icon-heart" />
                            </a>
                            <a href="#" className="add-to-cart" tabIndex={0}>
                              <i className="icon-handbag" />
                            </a>
                            <a
                              href="#"
                              className="quick-view"
                              data-toggle="modal"
                              data-target="#exampleModalCenter"
                              tabIndex={0}
                            >
                              <i className="icon-shuffle" />
                            </a>
                          </div>
                        </div>
                        <div className="product-content">
                          <h3>
                            <a href="product-details.html" tabIndex={0}>
                              Products Name 003
                            </a>
                          </h3>
                          <div className="price-box">
                            <span className="old-price">144.00</span>
                            <span className="new-price">124.00</span>
                          </div>
                        </div>
                      </div>
                      {/* single-product-wrap end */}
                    </div>
                    <div
                      className="col slick-slide slick-active"
                      data-slick-index={2}
                      aria-hidden="false"
                      style={{ width: 300 }}
                      tabIndex={0}
                    >
                      {/* single-product-wrap start */}
                      <div className="single-product-wrap">
                        <div className="product-image">
                          <a href="product-details.html" tabIndex={0}>
                            <img
                              src={require("../assets/images/product/11.jpg")}
                              alt=""
                            />
                          </a>
                          <div className="product-action">
                            <a href="#" className="wishlist" tabIndex={0}>
                              <i className="icon-heart" />
                            </a>
                            <a href="#" className="add-to-cart" tabIndex={0}>
                              <i className="icon-handbag" />
                            </a>
                            <a
                              href="#"
                              className="quick-view"
                              data-toggle="modal"
                              data-target="#exampleModalCenter"
                              tabIndex={0}
                            >
                              <i className="icon-shuffle" />
                            </a>
                          </div>
                        </div>
                        <div className="product-content">
                          <h3>
                            <a href="product-details.html" tabIndex={0}>
                              Products Name 005
                            </a>
                          </h3>
                          <div className="price-box">
                            <span className="old-price">130.00</span>
                            <span className="new-price">120.00</span>
                          </div>
                        </div>
                      </div>
                      {/* single-product-wrap end */}
                    </div>
                    <div
                      className="col slick-slide slick-active"
                      data-slick-index={3}
                      aria-hidden="false"
                      style={{ width: 300 }}
                      tabIndex={0}
                    >
                      {/* single-product-wrap start */}
                      <div className="single-product-wrap">
                        <div className="product-image">
                          <a href="product-details.html" tabIndex={0}>
                            <img
                              src={require("../assets/images/product/12.jpg")}
                              alt=""
                            />
                          </a>
                          <div className="product-action">
                            <a href="#" className="wishlist" tabIndex={0}>
                              <i className="icon-heart" />
                            </a>
                            <a href="#" className="add-to-cart" tabIndex={0}>
                              <i className="icon-handbag" />
                            </a>
                            <a
                              href="#"
                              className="quick-view"
                              data-toggle="modal"
                              data-target="#exampleModalCenter"
                              tabIndex={0}
                            >
                              <i className="icon-shuffle" />
                            </a>
                          </div>
                        </div>
                        <div className="product-content">
                          <h3>
                            <a href="product-details.html" tabIndex={0}>
                              Products Name 012
                            </a>
                          </h3>
                          <div className="price-box">
                            <span className="new-price">120.00</span>
                          </div>
                        </div>
                      </div>
                      {/* single-product-wrap end */}
                    </div>
                    <div
                      className="col slick-slide"
                      data-slick-index={4}
                      aria-hidden="true"
                      style={{ width: 300 }}
                      tabIndex={-1}
                    >
                      {/* single-product-wrap start */}
                      <div className="single-product-wrap">
                        <div className="product-image">
                          <a href="product-details.html" tabIndex={-1}>
                            <img
                              src={require("../assets/images/product/3.jpg")}
                              alt=""
                            />
                          </a>
                          <div className="product-action">
                            <a href="#" className="wishlist" tabIndex={-1}>
                              <i className="icon-heart" />
                            </a>
                            <a href="#" className="add-to-cart" tabIndex={-1}>
                              <i className="icon-handbag" />
                            </a>
                            <a
                              href="#"
                              className="quick-view"
                              data-toggle="modal"
                              data-target="#exampleModalCenter"
                              tabIndex={-1}
                            >
                              <i className="icon-shuffle" />
                            </a>
                          </div>
                        </div>
                        <div className="product-content">
                          <h3>
                            <a href="product-details.html" tabIndex={-1}>
                              Products Name 001
                            </a>
                          </h3>
                          <div className="price-box">
                            <span className="old-price">140.00</span>
                            <span className="new-price">120.00</span>
                          </div>
                        </div>
                      </div>
                      {/* single-product-wrap end */}
                    </div>
                    <div
                      className="col slick-slide"
                      data-slick-index={5}
                      aria-hidden="true"
                      style={{ width: 300 }}
                      tabIndex={-1}
                    >
                      {/* single-product-wrap start */}
                      <div className="single-product-wrap">
                        <div className="product-image">
                          <Link to="/details" tabIndex={-1}>
                            <img
                              src={require("../assets/images/product/7.jpg")}
                              alt=""
                            />
                          </Link>
                          <div className="product-action">
                            <Link to="/details" tabIndex={-1}>
                              <i className="icon-heart" />
                            </Link>
                            <Link
                              to="/cart"
                              tabIndex={-1}
                              className="add-to-cart"
                              tabIndex={-1}
                            >
                              <i className="icon-handbag" />
                            </Link>
                            <a
                              href="#"
                              className="quick-view"
                              data-toggle="modal"
                              data-target="#exampleModalCenter"
                              tabIndex={-1}
                            >
                              <i className="icon-shuffle" />
                            </a>
                          </div>
                        </div>
                        <div className="product-content">
                          <h3>
                            <a href="product-details.html" tabIndex={-1}>
                              Products Name 001
                            </a>
                          </h3>
                          <div className="price-box">
                            <span className="old-price">140.00</span>
                            <span className="new-price">120.00</span>
                          </div>
                        </div>
                      </div>
                      {/* single-product-wrap end */}
                    </div>
                    <div
                      className="col slick-slide slick-cloned"
                      data-slick-index={6}
                      aria-hidden="true"
                      style={{ width: 300 }}
                      tabIndex={-1}
                    >
                      {/* single-product-wrap start */}
                      <div className="single-product-wrap">
                        <div className="product-image">
                          <a href="product-details.html" tabIndex={-1}>
                            <img
                              src={require("../assets/images/product/9.jpg")}
                              alt=""
                            />
                          </a>
                          <div className="product-action">
                            <a href="#" className="wishlist" tabIndex={-1}>
                              <i className="icon-heart" />
                            </a>
                            <a href="#" className="add-to-cart" tabIndex={-1}>
                              <i className="icon-handbag" />
                            </a>
                            <a
                              href="#"
                              className="quick-view"
                              data-toggle="modal"
                              data-target="#exampleModalCenter"
                              tabIndex={-1}
                            >
                              <i className="icon-shuffle" />
                            </a>
                          </div>
                        </div>
                        <div className="product-content">
                          <h3>
                            <a href="product-details.html" tabIndex={-1}>
                              Products Name 009
                            </a>
                          </h3>
                          <div className="price-box">
                            <span className="old-price">150.00</span>
                            <span className="new-price">125.00</span>
                          </div>
                        </div>
                      </div>
                      {/* single-product-wrap end */}
                    </div>
                    <div
                      className="col slick-slide slick-cloned"
                      data-slick-index={7}
                      aria-hidden="true"
                      style={{ width: 300 }}
                      tabIndex={-1}
                    >
                      {/* single-product-wrap start */}
                      <div className="single-product-wrap">
                        <div className="product-image">
                          <a href="product-details.html" tabIndex={-1}>
                            <img
                              src={require("../assets/images/product/10.jpg")}
                              alt=""
                            />
                          </a>
                          <div className="product-action">
                            <a href="#" className="wishlist" tabIndex={-1}>
                              <i className="icon-heart" />
                            </a>
                            <a href="#" className="add-to-cart" tabIndex={-1}>
                              <i className="icon-handbag" />
                            </a>
                            <a
                              href="#"
                              className="quick-view"
                              data-toggle="modal"
                              data-target="#exampleModalCenter"
                              tabIndex={-1}
                            >
                              <i className="icon-shuffle" />
                            </a>
                          </div>
                        </div>
                        <div className="product-content">
                          <h3>
                            <a href="product-details.html" tabIndex={-1}>
                              Products Name 003
                            </a>
                          </h3>
                          <div className="price-box">
                            <span className="old-price">144.00</span>
                            <span className="new-price">124.00</span>
                          </div>
                        </div>
                      </div>
                      {/* single-product-wrap end */}
                    </div>
                    <div
                      className="col slick-slide slick-cloned"
                      data-slick-index={8}
                      aria-hidden="true"
                      style={{ width: 300 }}
                      tabIndex={-1}
                    >
                      {/* single-product-wrap start */}
                      <div className="single-product-wrap">
                        <div className="product-image">
                          <a href="product-details.html" tabIndex={-1}>
                            <img
                              src={require("../assets/images/product/11.jpg")}
                              alt=""
                            />
                          </a>
                          <div className="product-action">
                            <a href="#" className="wishlist" tabIndex={-1}>
                              <i className="icon-heart" />
                            </a>
                            <a href="#" className="add-to-cart" tabIndex={-1}>
                              <i className="icon-handbag" />
                            </a>
                            <a
                              href="#"
                              className="quick-view"
                              data-toggle="modal"
                              data-target="#exampleModalCenter"
                              tabIndex={-1}
                            >
                              <i className="icon-shuffle" />
                            </a>
                          </div>
                        </div>
                        <div className="product-content">
                          <h3>
                            <a href="product-details.html" tabIndex={-1}>
                              Products Name 005
                            </a>
                          </h3>
                          <div className="price-box">
                            <span className="old-price">130.00</span>
                            <span className="new-price">120.00</span>
                          </div>
                        </div>
                      </div>
                      {/* single-product-wrap end */}
                    </div>
                    <div
                      className="col slick-slide slick-cloned"
                      data-slick-index={9}
                      aria-hidden="true"
                      style={{ width: 300 }}
                      tabIndex={-1}
                    >
                      {/* single-product-wrap start */}
                      <div className="single-product-wrap">
                        <div className="product-image">
                          <a href="product-details.html" tabIndex={-1}>
                            <img
                              src={require("../assets/images/product/12.jpg")}
                              alt=""
                            />
                          </a>
                          <div className="product-action">
                            <a href="#" className="wishlist" tabIndex={-1}>
                              <i className="icon-heart" />
                            </a>
                            <a href="#" className="add-to-cart" tabIndex={-1}>
                              <i className="icon-handbag" />
                            </a>
                            <a
                              href="#"
                              className="quick-view"
                              data-toggle="modal"
                              data-target="#exampleModalCenter"
                              tabIndex={-1}
                            >
                              <i className="icon-shuffle" />
                            </a>
                          </div>
                        </div>
                        <div className="product-content">
                          <h3>
                            <a href="product-details.html" tabIndex={-1}>
                              Products Name 012
                            </a>
                          </h3>
                          <div className="price-box">
                            <span className="new-price">120.00</span>
                          </div>
                        </div>
                      </div>
                      {/* single-product-wrap end */}
                    </div>
                    <div
                      className="col slick-slide slick-cloned"
                      data-slick-index={10}
                      aria-hidden="true"
                      style={{ width: 300 }}
                      tabIndex={-1}
                    >
                      {/* single-product-wrap start */}
                      <div className="single-product-wrap">
                        <div className="product-image">
                          <a href="product-details.html" tabIndex={-1}>
                            <img
                              src={require("../assets/images/product/3.jpg")}
                              alt=""
                            />
                          </a>
                          <div className="product-action">
                            <a href="#" className="wishlist" tabIndex={-1}>
                              <i className="icon-heart" />
                            </a>
                            <a href="#" className="add-to-cart" tabIndex={-1}>
                              <i className="icon-handbag" />
                            </a>
                            <a
                              href="#"
                              className="quick-view"
                              data-toggle="modal"
                              data-target="#exampleModalCenter"
                              tabIndex={-1}
                            >
                              <i className="icon-shuffle" />
                            </a>
                          </div>
                        </div>
                        <div className="product-content">
                          <h3>
                            <a href="product-details.html" tabIndex={-1}>
                              Products Name 001
                            </a>
                          </h3>
                          <div className="price-box">
                            <span className="old-price">140.00</span>
                            <span className="new-price">120.00</span>
                          </div>
                        </div>
                      </div>
                      {/* single-product-wrap end */}
                    </div>
                    <div
                      className="col slick-slide slick-cloned"
                      data-slick-index={11}
                      aria-hidden="true"
                      style={{ width: 300 }}
                      tabIndex={-1}
                    >
                      {/* single-product-wrap start */}
                      <div className="single-product-wrap">
                        <div className="product-image">
                          <a href="product-details.html" tabIndex={-1}>
                            <img
                              src={require("../assets/images/product/7.jpg")}
                              alt=""
                            />
                          </a>
                          <div className="product-action">
                            <a href="#" className="wishlist" tabIndex={-1}>
                              <i className="icon-heart" />
                            </a>
                            <a href="#" className="add-to-cart" tabIndex={-1}>
                              <i className="icon-handbag" />
                            </a>
                            <a
                              href="#"
                              className="quick-view"
                              data-toggle="modal"
                              data-target="#exampleModalCenter"
                              tabIndex={-1}
                            >
                              <i className="icon-shuffle" />
                            </a>
                          </div>
                        </div>
                        <div className="product-content">
                          <h3>
                            <a href="product-details.html" tabIndex={-1}>
                              Products Name 001
                            </a>
                          </h3>
                          <div className="price-box">
                            <span className="old-price">140.00</span>
                            <span className="new-price">120.00</span>
                          </div>
                        </div>
                      </div>
                      {/* single-product-wrap end */}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* product-warpper start */}
          </div>
        </div>

        <div className="our-service-area pb--70">
          <div className="container">
            <div className="row">
              <div className="col-lg-4 col-md-4">
                <div className="single-service text-center mb--30">
                  <div className="service-icon">
                    <img
                      src={require("../assets/images/icon/s-2.png")}
                      alt=""
                    />
                  </div>
                  <div className="service-content">
                    <h3>Free Home Delivary.</h3>
                    <p>
                      Lorem ipsum dolor sit amet consec adi elit sed do eiusmod
                      tempor
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-md-4">
                <div className="single-service text-center mb--30">
                  <div className="service-icon">
                    <img
                      src={require("../assets/images/icon/s-1.png")}
                      alt=""
                    />
                  </div>
                  <div className="service-content">
                    <h3>Free Home Delivary.</h3>
                    <p>
                      Lorem ipsum dolor sit amet consec adi elit sed do eiusmod
                      tempor
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-md-4">
                <div className="single-service text-center mb--30">
                  <div className="service-icon">
                    <img
                      src={require("../assets/images/icon/s-3.png")}
                      alt=""
                    />
                  </div>
                  <div className="service-content">
                    <h3>24 Hour Support.</h3>
                    <p>
                      Lorem ipsum dolor sit amet consec adi elit sed do eiusmod
                      tempor
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="testimonial-area testimonial-bg overlay section-ptb">
          <div className="container">
            <div className="row">
              <div className="col-lg-8 offset-md-2 col-md-8 col-sm-12">
                <div className="testimonial-slider slick-initialized slick-slider">
                  <div className="slick-list draggable">
                    <div
                      className="slick-track"
                      style={{
                        opacity: 1,
                        width: 3850,
                        transform: "translate3d(-1540px, 0px, 0px)",
                        transition: "transform 500ms ease 0s",
                      }}
                    >
                      <div
                        className="testimonial-inner text-center slick-slide slick-cloned"
                        data-slick-index={-1}
                        aria-hidden="true"
                        style={{ width: 770 }}
                        tabIndex={-1}
                      >
                        <div className="test-cont">
                          <img
                            src={require("../assets/images/icon/2.png")}
                            alt=""
                          />
                          <p>
                            There are many variations of passages of Lorem Ipsum
                            available, but the majority have suffered alteration
                            in some form. There are many variations of passages.
                          </p>
                        </div>
                        <div className="test-author">
                          <h4>Michelle Mitchell</h4>
                          <p>ui ux - Designer</p>
                        </div>
                      </div>
                      <div
                        className="testimonial-inner text-center slick-slide"
                        data-slick-index={0}
                        aria-hidden="true"
                        style={{ width: 770 }}
                        tabIndex={0}
                      >
                        <div className="test-cont">
                          <img
                            src={require("../assets/images/icon/2.png")}
                            alt=""
                          />
                          <p>
                            There are many variations of passages of Lorem Ipsum
                            available, but the majority have suffered alteration
                            in some form. There are many variations of passages.
                          </p>
                        </div>
                        <div className="test-author">
                          <h4>Michelle Mitchell</h4>
                          <p>ui ux - Designer</p>
                        </div>
                      </div>
                      <div
                        className="testimonial-inner text-center slick-slide slick-current slick-active"
                        data-slick-index={1}
                        aria-hidden="false"
                        style={{ width: 770 }}
                        tabIndex={-1}
                      >
                        <div className="test-cont">
                          <img
                            src={require("../assets/images/icon/2.png")}
                            alt=""
                          />
                          <p>
                            There are many variations of passages of Lorem Ipsum
                            available, but the majority have suffered alteration
                            in some form. There are many variations of passages.
                          </p>
                        </div>
                        <div className="test-author">
                          <h4>Michelle Mitchell</h4>
                          <p>ui ux - Designer</p>
                        </div>
                      </div>
                      <div
                        className="testimonial-inner text-center slick-slide slick-cloned"
                        data-slick-index={2}
                        aria-hidden="true"
                        style={{ width: 770 }}
                        tabIndex={-1}
                      >
                        <div className="test-cont">
                          <img
                            src={require("../assets/images/icon/2.png")}
                            alt=""
                          />
                          <p>
                            There are many variations of passages of Lorem Ipsum
                            available, but the majority have suffered alteration
                            in some form. There are many variations of passages.
                          </p>
                        </div>
                        <div className="test-author">
                          <h4>Michelle Mitchell</h4>
                          <p>ui ux - Designer</p>
                        </div>
                      </div>
                      <div
                        className="testimonial-inner text-center slick-slide slick-cloned"
                        data-slick-index={3}
                        aria-hidden="true"
                        style={{ width: 770 }}
                        tabIndex={-1}
                      >
                        <div className="test-cont">
                          <img
                            src={require("../assets/images/icon/2.png")}
                            alt=""
                          />
                          <p>
                            There are many variations of passages of Lorem Ipsum
                            available, but the majority have suffered alteration
                            in some form. There are many variations of passages.
                          </p>
                        </div>
                        <div className="test-author">
                          <h4>Michelle Mitchell</h4>
                          <p>ui ux - Designer</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="footer-top pt--40 pb--100">
          <div className="container">
            <div className="row">
              <div className="col-lg-4 col-md-12">
                <div className="footer-info mt--60">
                  <div className="footer-title">
                    <h3>My Account</h3>
                  </div>
                  <ul className="footer-info-list">
                    <li>
                      <i className="ion-ios-location-outline" /> 184 Main Rd E,
                      St Albans VIC 3021, Australia
                    </li>
                    <li>
                      <i className="ion-ios-email-outline" /> Mill Us :{" "}
                      <a href="#">yourmail@gmail.com</a>
                    </li>
                    <li>
                      <i className="ion-ios-telephone-outline" /> Phone: + 00
                      254 254565 54
                    </li>
                  </ul>
                  <div className="payment-cart">
                    <img src="assets/images/icon/1.png" alt />
                  </div>
                </div>
              </div>
              <div className="col-lg-2 col-md-4">
                <div className="footer-info mt--60">
                  <div className="footer-title">
                    <h3>Categories</h3>
                  </div>
                  <ul className="footer-list">
                    <li>
                      <a href="#">Ecommerce</a>
                    </li>
                    <li>
                      <a href="#">Shopify</a>
                    </li>
                    <li>
                      <a href="#">Prestashop</a>
                    </li>
                    <li>
                      <a href="#">Opencart</a>
                    </li>
                    <li>
                      <a href="#">Magento</a>
                    </li>
                    <li>
                      <a href="#">Jigoshop</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-lg-2 offset-lg-1 col-md-4">
                <div className="footer-info mt--60">
                  <div className="footer-title">
                    <h3>Information</h3>
                  </div>
                  <ul className="footer-list">
                    <li>
                      <a href="#">Home</a>
                    </li>
                    <li>
                      <a href="#">About Us</a>
                    </li>
                    <li>
                      <a href="#">Contact Us</a>
                    </li>
                    <li>
                      <a href="#">Returns &amp; Exchanges</a>
                    </li>
                    <li>
                      <a href="#">Shipping &amp; Delivery</a>
                    </li>
                    <li>
                      <a href="#">Privacy Policy</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-lg-2 offset-lg-1 col-md-4">
                <div className="footer-info mt--60">
                  <div className="footer-title">
                    <h3>Quick Links</h3>
                  </div>
                  <ul className="footer-list">
                    <li>
                      <a href="#">Store Location</a>
                    </li>
                    <li>
                      <a href="#">My Account</a>
                    </li>
                    <li>
                      <a href="#">Orders Tracking</a>
                    </li>
                    <li>
                      <a href="#">Size Guide</a>
                    </li>
                    <li>
                      <a href="#">Shopping Rates</a>
                    </li>
                    <li>
                      <a href="#">Contact Us</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Main div */}
      </div>
    );
  }
}
