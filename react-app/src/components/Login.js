import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class Login extends Component {
  render() {
    return (
      <div>
        <div className="main-content-wrap section-ptb lagin-and-register-page">
          <div className="container">
            <div className="row">
              <div className="col-lg-7 col-md-12 ml-auto mr-auto">
                <div className="login-register-wrapper">
                  {/* login-register-tab-list start */}
                  <div className="login-register-tab-list nav">
                    <a className="active" data-toggle="tab" href="#lg1">
                      <h4> login </h4>
                    </a>
                    <Link to="/signup">
                      <h4> register </h4>
                    </Link>
                  </div>
                  {/* login-register-tab-list end */}
                  <div className="tab-content">
                    <div id="lg1" className="tab-pane active">
                      <div className="login-form-container">
                        <div className="login-register-form">
                          <form action="#" method="post">
                            <div className="login-input-box">
                              <input
                                type="text"
                                name="user-name"
                                placeholder="User Name"
                              />
                              <input
                                type="password"
                                name="user-password"
                                placeholder="Password"
                              />
                            </div>
                            <div className="button-box">
                              <div className="login-toggle-btn">
                                <input type="checkbox" />
                                <label>Remember me</label>
                                <a href="#">Forgot Password?</a>
                              </div>
                              <div className="button-box">
                                <button className="login-btn btn" type="submit">
                                  <span>Login</span>
                                </button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                    <div id="lg2" className="tab-pane">
                      <div className="login-form-container">
                        <div className="login-register-form">
                          <form action="#" method="post">
                            <div className="login-input-box">
                              <input
                                type="text"
                                name="user-name"
                                placeholder="User Name"
                              />
                              <input
                                type="password"
                                name="user-password"
                                placeholder="Password"
                              />
                              <input
                                name="user-email"
                                placeholder="Email"
                                type="email"
                              />
                            </div>
                            <div className="button-box">
                              <button
                                className="register-btn btn"
                                type="submit"
                              >
                                <span>Register</span>
                              </button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer className="footer-area">
          <div className="footer-top pt--40 pb--100">
            <div className="container">
              <div className="row">
                <div className="col-lg-4 col-md-12">
                  <div className="footer-info mt--60">
                    <div className="footer-title">
                      <h3>My Account</h3>
                    </div>
                    <ul className="footer-info-list">
                      <li>
                        <i className="ion-ios-location-outline" /> 184 Main Rd
                        E, St Albans VIC 3021, Australia
                      </li>
                      <li>
                        <i className="ion-ios-email-outline" /> Mill Us :{" "}
                        <a href="#">yourmail@gmail.com</a>
                      </li>
                      <li>
                        <i className="ion-ios-telephone-outline" /> Phone: + 00
                        254 254565 54
                      </li>
                    </ul>
                    <div className="payment-cart">
                      <img src="assets/images/icon/1.png" alt />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </footer>
      </div>
    );
  }
}
