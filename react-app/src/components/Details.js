import React, { Component } from "react";

export default class Details extends Component {
  render() {
    return (
      <div>
        <div className="breadcrumb-area section-ptb">
          <div className="container">
            <div className="row">
              <div className="col">
                <h2 className="breadcrumb-title">Product Details</h2>
                {/* breadcrumb-list start */}
                <ul className="breadcrumb-list">
                  <li className="breadcrumb-item">
                    <a href="index.html">Home</a>
                  </li>
                  <li className="breadcrumb-item active">Product Details</li>
                </ul>
                {/* breadcrumb-list end */}
              </div>
            </div>
          </div>
        </div>
        <div className="main-content-wrap section-ptb product-details-page">
          <div className="container">
            <div className="row">
              <div className="col-xl-5 col-lg-7 col-md-6">
                <div className="product-details-images">
                  <div className="product_details_container">
                    {/* product_big_images start */}
                    <div className="product_big_images-top">
                      <div className="portfolio-full-image tab-content">
                        <div
                          role="tabpanel"
                          className="tab-pane active product-image-position"
                          id="img-tab-5"
                        >
                          <a
                            href="assets/images/product-details/big-img/w1.jpg"
                            className="img-poppu"
                          >
                            <img
                              src={require("../assets/images/product-details/big-img/w1.jpg")}
                              alt="#"
                            />
                          </a>
                        </div>
                        <div
                          role="tabpanel"
                          className="tab-pane product-image-position"
                          id="img-tab-6"
                        >
                          <a
                            href="assets/images/product-details/big-img/w2.jpg"
                            className="img-poppu"
                          >
                            <img
                              src="assets/images/product-details/big-img/w2.jpg"
                              alt="#"
                            />
                          </a>
                        </div>
                        <div
                          role="tabpanel"
                          className="tab-pane product-image-position"
                          id="img-tab-7"
                        >
                          <a
                            href="assets/images/product-details/big-img/w3.jpg"
                            className="img-poppu"
                          >
                            <img
                              src="assets/images/product-details/big-img/w3.jpg"
                              alt="#"
                            />
                          </a>
                        </div>
                        <div
                          role="tabpanel"
                          className="tab-pane product-image-position"
                          id="img-tab-8"
                        >
                          <a
                            href="assets/images/product-details/big-img/w4.jpg"
                            className="img-poppu"
                          >
                            <img
                              src="assets/images/product-details/big-img/w4.jpg"
                              alt="#"
                            />
                          </a>
                        </div>
                        <div
                          role="tabpanel"
                          className="tab-pane product-image-position"
                          id="img-tab-9"
                        >
                          <a
                            href="assets/images/product-details/big-img/w2.jpg"
                            className="img-poppu"
                          >
                            <img
                              src="assets/images/product-details/big-img/w2.jpg"
                              alt="#"
                            />
                          </a>
                        </div>
                      </div>
                    </div>
                    {/* product_big_images end */}
                    {/* Start Small images */}
                    <ul
                      className="product_small_images-bottom horizantal-product-active nav slick-initialized slick-slider"
                      role="tablist"
                    >
                      <i
                        className="icon-arrow-top arrow-prv slick-arrow"
                        style={{ display: "inline" }}
                      />
                      <div className="slick-list draggable">
                        <div
                          className="slick-track"
                          style={{
                            opacity: 1,
                            width: 1708,
                            transform: "translate3d(-488px, 0px, 0px)",
                          }}
                        >
                          <li
                            role="presentation"
                            className="pot-small-img slick-slide slick-cloned"
                            data-slick-index={-4}
                            aria-hidden="true"
                            style={{ width: 107 }}
                            tabIndex={-1}
                          >
                            <a
                              href="#img-tab-6"
                              role="tab"
                              data-toggle="tab"
                              tabIndex={-1}
                            >
                              <img
                                src="assets/images/product-details/small-img/2.jpg"
                                alt="small-image"
                              />
                            </a>
                          </li>
                          <li
                            role="presentation"
                            className="pot-small-img slick-slide slick-cloned"
                            data-slick-index={-3}
                            aria-hidden="true"
                            style={{ width: 107 }}
                            tabIndex={-1}
                          >
                            <a
                              href="#img-tab-7"
                              role="tab"
                              data-toggle="tab"
                              tabIndex={-1}
                            >
                              <img
                                src={require("../assets/images/product-details/small-img/3.jpg")}
                                alt="small-image"
                              />
                            </a>
                          </li>
                          <li
                            role="presentation"
                            className="pot-small-img slick-slide slick-cloned"
                            data-slick-index={-2}
                            aria-hidden="true"
                            style={{ width: 107 }}
                            tabIndex={-1}
                          >
                            <a
                              href="#img-tab-8"
                              role="tab"
                              data-toggle="tab"
                              tabIndex={-1}
                            >
                              <img
                                src={require("../assets/images/product-details/small-img/4.jpg")}
                                alt="small-image"
                              />
                            </a>
                          </li>
                          <li
                            role="presentation"
                            className="pot-small-img slick-slide slick-cloned"
                            data-slick-index={-1}
                            aria-hidden="true"
                            style={{ width: 107 }}
                            tabIndex={-1}
                          >
                            <a
                              href="#img-tab-9"
                              role="tab"
                              data-toggle="tab"
                              tabIndex={-1}
                            >
                              <img
                                src={require("../assets/images/product-details/small-img/2.jpg")}
                                alt="small-image"
                              />
                            </a>
                          </li>
                          <li
                            role="presentation"
                            className="pot-small-img active slick-slide slick-current slick-active"
                            data-slick-index={0}
                            aria-hidden="false"
                            style={{ width: 107 }}
                            tabIndex={0}
                          >
                            <a
                              href="#img-tab-5"
                              role="tab"
                              data-toggle="tab"
                              tabIndex={0}
                            >
                              <img
                                src={require("../assets/images/product-details/small-img/1.jpg")}
                                alt="small-image"
                              />
                            </a>
                          </li>
                          <li
                            role="presentation"
                            className="pot-small-img slick-slide slick-active"
                            data-slick-index={1}
                            aria-hidden="false"
                            style={{ width: 107 }}
                            tabIndex={0}
                          >
                            <a
                              href="#img-tab-6"
                              role="tab"
                              data-toggle="tab"
                              tabIndex={0}
                            >
                              <img
                                src={require("../assets/images/product-details/small-img/2.jpg")}
                                alt="small-image"
                              />
                            </a>
                          </li>
                          <li
                            role="presentation"
                            className="pot-small-img slick-slide slick-active"
                            data-slick-index={2}
                            aria-hidden="false"
                            style={{ width: 107 }}
                            tabIndex={0}
                          >
                            <a
                              href="#img-tab-7"
                              role="tab"
                              data-toggle="tab"
                              tabIndex={0}
                            >
                              <img
                                src={require("../assets/images/product-details/small-img/3.jpg")}
                                alt="small-image"
                              />
                            </a>
                          </li>
                          <li
                            role="presentation"
                            className="pot-small-img slick-slide slick-active"
                            data-slick-index={3}
                            aria-hidden="false"
                            style={{ width: 107 }}
                            tabIndex={0}
                          >
                            <a
                              href="#img-tab-8"
                              role="tab"
                              data-toggle="tab"
                              tabIndex={0}
                            >
                              <img
                                src={require("../assets/images/product-details/small-img/4.jpg")}
                                alt="small-image"
                              />
                            </a>
                          </li>
                          <li
                            role="presentation"
                            className="pot-small-img slick-slide"
                            data-slick-index={4}
                            aria-hidden="true"
                            style={{ width: 107 }}
                            tabIndex={-1}
                          >
                            <a
                              href="#img-tab-9"
                              role="tab"
                              data-toggle="tab"
                              tabIndex={-1}
                            >
                              <img
                                src={require("../assets/images/product-details/small-img/2.jpg")}
                                alt="small-image"
                              />
                            </a>
                          </li>
                          <li
                            role="presentation"
                            className="pot-small-img active slick-slide slick-cloned"
                            data-slick-index={5}
                            aria-hidden="true"
                            style={{ width: 107 }}
                            tabIndex={-1}
                          >
                            <a
                              href="#img-tab-5"
                              role="tab"
                              data-toggle="tab"
                              tabIndex={-1}
                            >
                              <img
                                src={require("../assets/images/product-details/small-img/1.jpg")}
                                alt="small-image"
                              />
                            </a>
                          </li>
                          <li
                            role="presentation"
                            className="pot-small-img slick-slide slick-cloned"
                            data-slick-index={6}
                            aria-hidden="true"
                            style={{ width: 107 }}
                            tabIndex={-1}
                          >
                            <a
                              href="#img-tab-6"
                              role="tab"
                              data-toggle="tab"
                              tabIndex={-1}
                            >
                              <img
                                src={require("../assets/images/product-details/small-img/2.jpg")}
                                alt="small-image"
                              />
                            </a>
                          </li>
                          <li
                            role="presentation"
                            className="pot-small-img slick-slide slick-cloned"
                            data-slick-index={7}
                            aria-hidden="true"
                            style={{ width: 107 }}
                            tabIndex={-1}
                          >
                            <a
                              href="#img-tab-7"
                              role="tab"
                              data-toggle="tab"
                              tabIndex={-1}
                            >
                              <img
                                src={require("../assets/images/product-details/small-img/3.jpg")}
                                alt="small-image"
                              />
                            </a>
                          </li>
                          <li
                            role="presentation"
                            className="pot-small-img slick-slide slick-cloned"
                            data-slick-index={8}
                            aria-hidden="true"
                            style={{ width: 107 }}
                            tabIndex={-1}
                          >
                            <a
                              href="#img-tab-8"
                              role="tab"
                              data-toggle="tab"
                              tabIndex={-1}
                            >
                              <img
                                src={require("../assets/images/product-details/small-img/4.jpg")}
                                alt="small-image"
                              />
                            </a>
                          </li>
                          <li
                            role="presentation"
                            className="pot-small-img slick-slide slick-cloned"
                            data-slick-index={9}
                            aria-hidden="true"
                            style={{ width: 107 }}
                            tabIndex={-1}
                          >
                            <a
                              href="#img-tab-9"
                              role="tab"
                              data-toggle="tab"
                              tabIndex={-1}
                            >
                              <img
                                src={require("../assets/images/product-details/small-img/2.jpg")}
                                alt="small-image"
                              />
                            </a>
                          </li>
                        </div>
                      </div>
                      <i
                        className="icon-arrow-bottom arrow-next slick-arrow"
                        style={{ display: "inline" }}
                      />
                    </ul>
                    {/* End Small images */}
                  </div>
                </div>
              </div>
              <div className="col-xl-7 col-lg-5 col-md-6">
                {/* product_details_info start */}
                <div className="product_details_info">
                  <h2>Black Clock</h2>
                  {/* pro_rating start */}
                  <div className="pro_rating d-flex">
                    <ul className="product-rating d-flex">
                      <li>
                        <span className="icon-star" />
                      </li>
                      <li>
                        <span className="icon-star" />
                      </li>
                      <li>
                        <span className="icon-star" />
                      </li>
                      <li>
                        <span className="icon-star" />
                      </li>
                      <li>
                        <span className="icon-star" />
                      </li>
                    </ul>
                    <span className="rat_qun"> (Based on 0 Ratings) </span>
                  </div>
                  {/* pro_rating end */}
                  {/* pro_details start */}
                  <div className="pro_details">
                    <p>
                      Lorem ipsum dolor sit amet consectetur adipisicing elit,
                      sed do eiusmod temf incididunt ut labore et dolore magna
                      aliqua. Ut enim ad minim veniam, nostr exercitation
                      ullamco laboris nisi ut aliquip ex ea.{" "}
                    </p>
                  </div>
                  {/* pro_details end */}
                  {/* pro_dtl_prize start */}
                  <ul className="pro_dtl_prize">
                    <li className="old_prize">$15.21</li>
                    <li> $10.00</li>
                  </ul>
                  {/* pro_dtl_prize end */}
                  {/* pro_dtl_color start*/}
                  <div className="pro_dtl_color">
                    <h2 className="title_2">Choose Colour</h2>
                    <ul className="pro_choose_color">
                      <li className="red">
                        <a href="#">
                          <i className="ion-record" />
                        </a>
                      </li>
                      <li className="blue">
                        <a href="#">
                          <i className="ion-record" />
                        </a>
                      </li>
                      <li className="perpal">
                        <a href="#">
                          <i className="ion-record" />
                        </a>
                      </li>
                      <li className="yellow">
                        <a href="#">
                          <i className="ion-record" />
                        </a>
                      </li>
                    </ul>
                  </div>
                  {/* pro_dtl_color end*/}
                  {/* pro_dtl_size start */}
                  <div className="pro_dtl_size">
                    <h2 className="title_2">Size</h2>
                    <ul className="pro_choose_size">
                      <li>
                        <a href="#">S</a>
                      </li>
                      <li>
                        <a href="#">M</a>
                      </li>
                      <li>
                        <a href="#">XL</a>
                      </li>
                      <li>
                        <a href="#">XXL</a>
                      </li>
                    </ul>
                  </div>
                  {/* pro_dtl_size end */}
                  {/* product-quantity-action start */}
                  <div className="product-quantity-action">
                    <div className="prodict-statas">
                      <span>Quantity :</span>
                    </div>
                    <div className="product-quantity">
                      <form action="#">
                        <div className="product-quantity">
                          <div className="cart-plus-minus">
                            <input defaultValue={"01"} type="number" />
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  {/* product-quantity-action end */}
                  {/* pro_dtl_btn start */}
                  <ul className="pro_dtl_btn">
                    <li>
                      <a href="#" className="buy_now_btn">
                        buy now
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="ion-heart" />
                      </a>
                    </li>
                  </ul>
                  {/* pro_dtl_btn end */}
                  {/* pro_social_share start */}
                  <div className="pro_social_share d-flex">
                    <h2 className="title_2">Share :</h2>
                    <ul className="pro_social_link">
                      <li>
                        <a href="#">
                          <i className="ion-social-twitter" />
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="ion-social-tumblr" />
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="ion-social-facebook" />
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="ion-social-instagram-outline" />
                        </a>
                      </li>
                    </ul>
                  </div>
                  {/* pro_social_share end */}
                </div>
                {/* product_details_info end */}
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div className="product-details-tab mt--60">
                  <ul role="tablist" className="mb--50 nav">
                    <li className="active" role="presentation">
                      <a
                        data-toggle="tab"
                        role="tab"
                        href="#description"
                        className="active"
                      >
                        Description
                      </a>
                    </li>
                    <li role="presentation">
                      <a data-toggle="tab" role="tab" href="#sheet">
                        Data sheet
                      </a>
                    </li>
                    <li role="presentation">
                      <a data-toggle="tab" role="tab" href="#reviews">
                        Reviews
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-12">
                <div className="product_details_tab_content tab-content">
                  {/* Start Single Content */}
                  <div
                    className="product_tab_content tab-pane active"
                    id="description"
                    role="tabpanel"
                  >
                    <div className="product_description_wrap">
                      <div className="product_desc mb--30">
                        <h2 className="title_3">Details</h2>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipisicing
                          elit, sed do eiusmod tempor incididunt ut labore et
                          dolore magna aliqua. Ut enim ad minim veniam, quis
                          noexercit ullamco laboris nisi ut aliquip ex ea
                          commodo consequat. Duis aute irure dolor in
                          reprehenderit in voluptate velit esse cillum dolore eu
                          fugiat nulla pariatur. Excepteur sint occaecat
                          cupidatat non proident, sunt in culpa qui officia
                          deserunt mollit anim id.
                        </p>
                      </div>
                      <div className="pro_feature">
                        <h2 className="title_3">Features</h2>
                        <ul className="feature_list">
                          <li>
                            <a href="#">
                              <i className="ion-ios-play-outline" />
                              Duis aute irure dolor in reprehenderit in
                              voluptate velit esse
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <i className="ion-ios-play-outline" />
                              Irure dolor in reprehenderit in voluptate velit
                              esse
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <i className="ion-ios-play-outline" />
                              Sed do eiusmod tempor incididunt ut labore et{" "}
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <i className="ion-ios-play-outline" />
                              Nisi ut aliquip ex ea commodo consequat.
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  {/* End Single Content */}
                  {/* Start Single Content */}
                  <div
                    className="product_tab_content tab-pane"
                    id="sheet"
                    role="tabpanel"
                  >
                    <div className="pro_feature">
                      <h2 className="title_3">Data sheet</h2>
                      <ul className="feature_list">
                        <li>
                          <a href="#">
                            <i className="ion-ios-play-outline" />
                            Duis aute irure dolor in reprehenderit in voluptate
                            velit esse
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i className="ion-ios-play-outline" />
                            Irure dolor in reprehenderit in voluptate velit esse
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i className="ion-ios-play-outline" />
                            Irure dolor in reprehenderit in voluptate velit esse
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i className="ion-ios-play-outline" />
                            Sed do eiusmod tempor incididunt ut labore et{" "}
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i className="ion-ios-play-outline" />
                            Sed do eiusmod tempor incididunt ut labore et{" "}
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i className="ion-ios-play-outline" />
                            Nisi ut aliquip ex ea commodo consequat.
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i className="ion-ios-play-outline" />
                            Nisi ut aliquip ex ea commodo consequat.
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  {/* End Single Content */}
                  {/* Start Single Content */}
                  <div
                    className="product_tab_content tab-pane"
                    id="reviews"
                    role="tabpanel"
                  >
                    <div className="review_address_inner">
                      {/* Start Single Review */}
                      <div className="pro_review">
                        <div className="review_thumb">
                          <img
                            alt="review images"
                            src={require("../assets/images/review/1.jpg")}
                          />
                        </div>
                        <div className="review_details">
                          <div className="review_info">
                            <h4>
                              <a href="#">Gerald Barnes</a>
                            </h4>
                            <ul className="product-rating d-flex">
                              <li>
                                <span className="icon-star" />
                              </li>
                              <li>
                                <span className="icon-star" />
                              </li>
                              <li>
                                <span className="icon-star" />
                              </li>
                              <li>
                                <span className="icon-star" />
                              </li>
                              <li>
                                <span className="icon-star" />
                              </li>
                            </ul>
                            <div className="rating_send">
                              <a href="#">
                                <i className="ion-reply" />
                              </a>
                            </div>
                          </div>
                          <div className="review_date">
                            <span>27 Jun, 2018 at 3:30pm</span>
                          </div>
                          <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Integer accumsan egestas elese ifend.
                            Phasellus a felis at estei to bibendum feugiat ut
                            eget eni Praesent et messages in con sectetur
                            posuere dolor non.
                          </p>
                        </div>
                      </div>
                      {/* End Single Review */}
                      {/* Start Single Review */}
                      <div className="pro_review ans">
                        <div className="review_thumb">
                          <img
                            alt="review images"
                            src={require("../assets/images/review/2.jpg")}
                          />
                        </div>
                        <div className="review_details">
                          <div className="review_info">
                            <h4>
                              <a href="#">Gerald Barnes</a>
                            </h4>
                            <ul className="product-rating d-flex">
                              <li>
                                <span className="icon-star" />
                              </li>
                              <li>
                                <span className="icon-star" />
                              </li>
                              <li>
                                <span className="icon-star" />
                              </li>
                              <li>
                                <span className="icon-star" />
                              </li>
                              <li>
                                <span className="icon-star" />
                              </li>
                            </ul>
                            <div className="rating_send">
                              <a href="#">
                                <i className="ion-reply" />
                              </a>
                            </div>
                          </div>
                          <div className="review_date">
                            <span>27 Jun, 2018 at 4:32pm</span>
                          </div>
                          <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Integer accumsan egestas elese ifend.
                            Phasellus a felis at estei to bibendum feugiat ut
                            eget eni Praesent et messages in con sectetur
                            posuere dolor non.
                          </p>
                        </div>
                      </div>
                      {/* End Single Review */}
                    </div>
                    {/* Start RAting Area */}
                    <div className="rating_wrap">
                      <h2 className="rating-title">Write A review</h2>
                      <h4 className="rating-title-2">Your Rating</h4>
                      <div className="rating_list">
                        <ul className="product-rating d-flex">
                          <li>
                            <span className="icon-star" />
                          </li>
                          <li>
                            <span className="icon-star" />
                          </li>
                          <li>
                            <span className="icon-star" />
                          </li>
                          <li>
                            <span className="icon-star" />
                          </li>
                          <li>
                            <span className="icon-star" />
                          </li>
                        </ul>
                      </div>
                    </div>
                    {/* End RAting Area */}
                    <div className="comments-area comments-reply-area">
                      <div className="row">
                        <div className="col-lg-12">
                          <form action="#" className="comment-form-area">
                            <div className="comment-input">
                              <p className="comment-form-author">
                                <label>
                                  Name <span className="required">*</span>
                                </label>
                                <input
                                  type="text"
                                  required="required"
                                  name="Name"
                                />
                              </p>
                              <p className="comment-form-email">
                                <label>
                                  Email <span className="required">*</span>
                                </label>
                                <input
                                  type="text"
                                  required="required"
                                  name="email"
                                />
                              </p>
                            </div>
                            <p className="comment-form-comment">
                              <label>Comment</label>
                              <textarea
                                className="comment-notes"
                                required="required"
                                defaultValue={"01"}
                              />
                            </p>
                            <div className="comment-form-submit">
                              <input
                                type="submit"
                                defaultValue="Post Comment"
                                className="comment-submit"
                              />
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* End Single Content */}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer-top pt--40 pb--100">
          <div className="container">
            <div className="row">
              <div className="col-lg-4 col-md-12">
                <div className="footer-info mt--60">
                  <div className="footer-title">
                    <h3>My Account</h3>
                  </div>
                  <ul className="footer-info-list">
                    <li>
                      <i className="ion-ios-location-outline" /> 184 Main Rd E,
                      St Albans VIC 3021, Australia
                    </li>
                    <li>
                      <i className="ion-ios-email-outline" /> Mill Us :{" "}
                      <a href="#">yourmail@gmail.com</a>
                    </li>
                    <li>
                      <i className="ion-ios-telephone-outline" /> Phone: + 00
                      254 254565 54
                    </li>
                  </ul>
                  <div className="payment-cart">
                    <img src="assets/images/icon/1.png" alt />
                  </div>
                </div>
              </div>
              <div className="col-lg-2 col-md-4">
                <div className="footer-info mt--60">
                  <div className="footer-title">
                    <h3>Categories</h3>
                  </div>
                  <ul className="footer-list">
                    <li>
                      <a href="#">Ecommerce</a>
                    </li>
                    <li>
                      <a href="#">Shopify</a>
                    </li>
                    <li>
                      <a href="#">Prestashop</a>
                    </li>
                    <li>
                      <a href="#">Opencart</a>
                    </li>
                    <li>
                      <a href="#">Magento</a>
                    </li>
                    <li>
                      <a href="#">Jigoshop</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-lg-2 offset-lg-1 col-md-4">
                <div className="footer-info mt--60">
                  <div className="footer-title">
                    <h3>Information</h3>
                  </div>
                  <ul className="footer-list">
                    <li>
                      <a href="#">Home</a>
                    </li>
                    <li>
                      <a href="#">About Us</a>
                    </li>
                    <li>
                      <a href="#">Contact Us</a>
                    </li>
                    <li>
                      <a href="#">Returns &amp; Exchanges</a>
                    </li>
                    <li>
                      <a href="#">Shipping &amp; Delivery</a>
                    </li>
                    <li>
                      <a href="#">Privacy Policy</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-lg-2 offset-lg-1 col-md-4">
                <div className="footer-info mt--60">
                  <div className="footer-title">
                    <h3>Quick Links</h3>
                  </div>
                  <ul className="footer-list">
                    <li>
                      <a href="#">Store Location</a>
                    </li>
                    <li>
                      <a href="#">My Account</a>
                    </li>
                    <li>
                      <a href="#">Orders Tracking</a>
                    </li>
                    <li>
                      <a href="#">Size Guide</a>
                    </li>
                    <li>
                      <a href="#">Shopping Rates</a>
                    </li>
                    <li>
                      <a href="#">Contact Us</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
