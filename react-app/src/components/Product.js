import React, { Component } from "react";
// import ProductList from "./";
import { Link } from "react-router-dom";

export default class Product extends Component {
  render() {
    return (
      <div>
        <div className="breadcrumb-area section-ptb">
          <div className="container">
            <div className="row">
              <div className="col">
                <h2 className="breadcrumb-title">Shop</h2>
                {/* breadcrumb-list start */}
                <ul className="breadcrumb-list">
                  <li className="breadcrumb-item">
                    <Link to="/">Home</Link>
                  </li>
                  <li className="breadcrumb-item active">Shop</li>
                </ul>
                {/* breadcrumb-list end */}
              </div>
            </div>
          </div>
        </div>
        <div className="main-content-wrap shop-page section-ptb">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                {/* shop-product-wrapper start */}
                <div className="shop-product-wrapper">
                  <div className="row">
                    <div className="col">
                      {/* shop-top-bar start */}
                      <div className="shop-top-bar">
                        {/* product-view-mode start */}
                        <div className="product-view-mode">
                          {/* shop-item-filter-list start */}
                          <ul
                            role="tablist"
                            className="nav shop-item-filter-list"
                          >
                            <li role="presentation" className="active">
                              <a
                                href="#grid"
                                aria-controls="grid"
                                role="tab"
                                data-toggle="tab"
                                className="active show"
                                aria-selected="true"
                              >
                                <i className="ion-ios-keypad-outline" />
                              </a>
                            </li>
                            <li role="presentation">
                              <a
                                href="#list"
                                aria-controls="list"
                                role="tab"
                                data-toggle="tab"
                              >
                                <i className="ion-ios-list-outline" />{" "}
                              </a>
                            </li>
                          </ul>
                          {/* shop-item-filter-list end */}
                        </div>
                        {/* product-view-mode end */}
                        {/* product-short start */}
                        <div className="product-short">
                          <p>Sort By:</p>
                          <select className="nice-select" name="sortby">
                            <option value="trending">Relevance</option>
                            <option value="sales">Name (A - Z)</option>
                            <option value="sales">Name (Z - A)</option>
                            <option value="rating">
                              Price (Low &gt; High)
                            </option>
                            <option value="date">Rating (Lowest)</option>
                            <option value="price-asc">Model (A - Z)</option>
                            <option value="price-asc">Model (Z - A)</option>
                          </select>
                        </div>
                        {/* product-short end */}
                      </div>
                      {/* shop-top-bar end */}
                    </div>
                  </div>
                  {/* shop-products-wrap start */}
                  <div className="shop-products-wrap">
                    <div className="tab-content">
                      <div
                        id="grid"
                        className="tab-pane fade active show"
                        role="tabpanel"
                      >
                        <div className="shop-product-wrap">
                          <div className="row">
                            <div className="col-lg-3 col-md-4 col-sm-6">
                              {/* single-product-wrap start */}
                              <div className="single-product-wrap">
                                <div className="product-image">
                                  <Link to="/details">
                                    <img
                                      src={require("../assets/images/product/1.jpg")}
                                      alt=""
                                    />
                                  </Link>
                                  <div className="product-action">
                                    <a href="#" className="wishlist">
                                      <i className="icon-heart" />
                                    </a>
                                    <a href="#" className="add-to-cart">
                                      <i className="icon-handbag" />
                                    </a>
                                    <a
                                      href="#"
                                      className="quick-view"
                                      data-toggle="modal"
                                      data-target="#exampleModalCenter"
                                    >
                                      <i className="icon-shuffle" />
                                    </a>
                                  </div>
                                </div>
                                <div className="product-content">
                                  <h3>
                                    <Link to="/details">Products Name 001</Link>
                                  </h3>
                                  <div className="price-box">
                                    <span className="old-price">140.00</span>
                                    <span className="new-price">120.00</span>
                                  </div>
                                </div>
                              </div>
                              {/* single-product-wrap end */}
                            </div>
                            <div className="col-lg-3 col-md-4 col-sm-6">
                              {/* single-product-wrap start */}
                              <div className="single-product-wrap">
                                <div className="product-image">
                                  <Link to="/details">
                                    <img
                                      src={require("../assets/images/product/2.jpg")}
                                      alt=""
                                    />
                                  </Link>
                                  <div className="product-action">
                                    <a href="#" className="wishlist">
                                      <i className="icon-heart" />
                                    </a>
                                    <a href="#" className="add-to-cart">
                                      <i className="icon-handbag" />
                                    </a>
                                    <a
                                      href="#"
                                      className="quick-view"
                                      data-toggle="modal"
                                      data-target="#exampleModalCenter"
                                    >
                                      <i className="icon-shuffle" />
                                    </a>
                                  </div>
                                </div>
                                <div className="product-content">
                                  <h3>
                                    <Link to="/details">Products Name 002</Link>
                                  </h3>
                                  <div className="price-box">
                                    <span className="new-price">120.00</span>
                                  </div>
                                </div>
                              </div>
                              {/* single-product-wrap end */}
                            </div>
                            <div className="col-lg-3 col-md-4 col-sm-6">
                              {/* single-product-wrap start */}
                              <div className="single-product-wrap">
                                <div className="product-image">
                                  <Link to="/details">
                                    <img
                                      src={require("../assets/images/product/3.jpg")}
                                      alt=""
                                    />
                                  </Link>
                                  <div className="product-action">
                                    <a href="#" className="wishlist">
                                      <i className="icon-heart" />
                                    </a>
                                    <a href="#" className="add-to-cart">
                                      <i className="icon-handbag" />
                                    </a>
                                    <a
                                      href="#"
                                      className="quick-view"
                                      data-toggle="modal"
                                      data-target="#exampleModalCenter"
                                    >
                                      <i className="icon-shuffle" />
                                    </a>
                                  </div>
                                </div>
                                <div className="product-content">
                                  <h3>
                                    <Link to="/details">Products Name 003</Link>
                                  </h3>
                                  <div className="price-box">
                                    <span className="old-price">230.00</span>
                                    <span className="new-price">210.00</span>
                                  </div>
                                </div>
                              </div>
                              {/* single-product-wrap end */}
                            </div>
                            <div className="col-lg-3 col-md-4 col-sm-6">
                              {/* single-product-wrap start */}
                              <div className="single-product-wrap">
                                <div className="product-image">
                                  <Link to="/details">
                                    <img
                                      src={require("../assets/images/product/4.jpg")}
                                      alt=""
                                    />
                                  </Link>
                                  <div className="product-action">
                                    <a href="#" className="wishlist">
                                      <i className="icon-heart" />
                                    </a>
                                    <a href="#" className="add-to-cart">
                                      <i className="icon-handbag" />
                                    </a>
                                    <a
                                      href="#"
                                      className="quick-view"
                                      data-toggle="modal"
                                      data-target="#exampleModalCenter"
                                    >
                                      <i className="icon-shuffle" />
                                    </a>
                                  </div>
                                </div>
                                <div className="product-content">
                                  <h3>
                                    <Link to="/details">Products Name 004</Link>
                                  </h3>
                                  <div className="price-box">
                                    <span className="new-price">120.00</span>
                                  </div>
                                </div>
                              </div>
                              {/* single-product-wrap end */}
                            </div>
                            <div className="col-lg-3 col-md-4 col-sm-6">
                              {/* single-product-wrap start */}
                              <div className="single-product-wrap">
                                <div className="product-image">
                                  <a href="#">
                                    <img
                                      src={require("../assets/images/product/5.jpg")}
                                      alt=""
                                    />
                                  </a>
                                  <div className="product-action">
                                    <a href="#" className="wishlist">
                                      <i className="icon-heart" />
                                    </a>
                                    <a href="#" className="add-to-cart">
                                      <i className="icon-handbag" />
                                    </a>
                                    <a
                                      href="#"
                                      className="quick-view"
                                      data-toggle="modal"
                                      data-target="#exampleModalCenter"
                                    >
                                      <i className="icon-shuffle" />
                                    </a>
                                  </div>
                                </div>
                                <div className="product-content">
                                  <h3>
                                    <Link to="/details">Products Name 005</Link>
                                  </h3>
                                  <div className="price-box">
                                    <span className="old-price">180.00</span>
                                    <span className="new-price">150.00</span>
                                  </div>
                                </div>
                              </div>
                              {/* single-product-wrap end */}
                            </div>
                            <div className="col-lg-3 col-md-4 col-sm-6">
                              {/* single-product-wrap start */}
                              <div className="single-product-wrap">
                                <div className="product-image">
                                  <Link to="/details">
                                    <img
                                      src={require("../assets/images/product/6.jpg")}
                                      alt=""
                                    />
                                  </Link>
                                  <div className="product-action">
                                    <a href="#" className="wishlist">
                                      <i className="icon-heart" />
                                    </a>
                                    <a href="#" className="add-to-cart">
                                      <i className="icon-handbag" />
                                    </a>
                                    <a
                                      href="#"
                                      className="quick-view"
                                      data-toggle="modal"
                                      data-target="#exampleModalCenter"
                                    >
                                      <i className="icon-shuffle" />
                                    </a>
                                  </div>
                                </div>
                                <div className="product-content">
                                  <h3>
                                    <Link to="/details">Products Name 006</Link>
                                  </h3>
                                  <div className="price-box">
                                    <span className="new-price">130.00</span>
                                  </div>
                                </div>
                              </div>
                              {/* single-product-wrap end */}
                            </div>
                            <div className="col-lg-3 col-md-4 col-sm-6">
                              {/* single-product-wrap start */}
                              <div className="single-product-wrap">
                                <div className="product-image">
                                  <Link to="/details">
                                    <img
                                      src={require("../assets/images/product/7.jpg")}
                                      alt=""
                                    />
                                  </Link>
                                  <div className="product-action">
                                    <a href="#" className="wishlist">
                                      <i className="icon-heart" />
                                    </a>
                                    <a href="#" className="add-to-cart">
                                      <i className="icon-handbag" />
                                    </a>
                                    <a
                                      href="#"
                                      className="quick-view"
                                      data-toggle="modal"
                                      data-target="#exampleModalCenter"
                                    >
                                      <i className="icon-shuffle" />
                                    </a>
                                  </div>
                                </div>
                                <div className="product-content">
                                  <h3>
                                    <Link to="/details">Products Name 007</Link>
                                  </h3>
                                  <div className="price-box">
                                    <span className="old-price">250.00</span>
                                    <span className="new-price">230.00</span>
                                  </div>
                                </div>
                              </div>
                              {/* single-product-wrap end */}
                            </div>
                            <div className="col-lg-3 col-md-4 col-sm-6">
                              {/* single-product-wrap start */}
                              <div className="single-product-wrap">
                                <div className="product-image">
                                  <Link to="/details">
                                    <img
                                      src={require("../assets/images/product/8.jpg")}
                                      alt=""
                                    />
                                  </Link>
                                  <div className="product-action">
                                    <a href="#" className="wishlist">
                                      <i className="icon-heart" />
                                    </a>
                                    <a href="#" className="add-to-cart">
                                      <i className="icon-handbag" />
                                    </a>
                                    <a
                                      href="#"
                                      className="quick-view"
                                      data-toggle="modal"
                                      data-target="#exampleModalCenter"
                                    >
                                      <i className="icon-shuffle" />
                                    </a>
                                  </div>
                                </div>
                                <div className="product-content">
                                  <h3>
                                    <Link to="/details">Products Name 008</Link>
                                  </h3>
                                  <div className="price-box">
                                    <span className="new-price">120.00</span>
                                  </div>
                                </div>
                              </div>
                              {/* single-product-wrap end */}
                            </div>
                            <div className="col-lg-3 col-md-4 col-sm-6">
                              {/* single-product-wrap start */}
                              <div className="single-product-wrap">
                                <div className="product-image">
                                  <Link to="/details">
                                    <img
                                      src={require("../assets/images/product/12.jpg")}
                                      alt=""
                                    />
                                  </Link>
                                  <div className="product-action">
                                    <a href="#" className="wishlist">
                                      <i className="icon-heart" />
                                    </a>
                                    <a href="#" className="add-to-cart">
                                      <i className="icon-handbag" />
                                    </a>
                                    <a
                                      href="#"
                                      className="quick-view"
                                      data-toggle="modal"
                                      data-target="#exampleModalCenter"
                                    >
                                      <i className="icon-shuffle" />
                                    </a>
                                  </div>
                                </div>
                                <div className="product-content">
                                  <h3>
                                    <Link to="/details">Products Name 012</Link>
                                  </h3>
                                  <div className="price-box">
                                    <span className="new-price">143.00</span>
                                  </div>
                                </div>
                              </div>
                              {/* single-product-wrap end */}
                            </div>
                            <div className="col-lg-3 col-md-4 col-sm-6">
                              {/* single-product-wrap start */}
                              <div className="single-product-wrap">
                                <div className="product-image">
                                  <Link to="/details">
                                    <img
                                      src={require("../assets/images/product/9.jpg")}
                                      alt=""
                                    />
                                  </Link>
                                  <div className="product-action">
                                    <a href="#" className="wishlist">
                                      <i className="icon-heart" />
                                    </a>
                                    <a href="#" className="add-to-cart">
                                      <i className="icon-handbag" />
                                    </a>
                                    <a
                                      href="#"
                                      className="quick-view"
                                      data-toggle="modal"
                                      data-target="#exampleModalCenter"
                                    >
                                      <i className="icon-shuffle" />
                                    </a>
                                  </div>
                                </div>
                                <div className="product-content">
                                  <h3>
                                    <Link to="/details">Products Name 008</Link>
                                  </h3>
                                  <div className="price-box">
                                    <span className="new-price">120.00</span>
                                  </div>
                                </div>
                              </div>
                              {/* single-product-wrap end */}
                            </div>
                            <div className="col-lg-3 col-md-4 col-sm-6">
                              {/* single-product-wrap start */}
                              <div className="single-product-wrap">
                                <div className="product-image">
                                  <Link to="/details">
                                    <img
                                      src={require("../assets/images/product/10.jpg")}
                                      alt=""
                                    />
                                  </Link>
                                  <div className="product-action">
                                    <a href="#" className="wishlist">
                                      <i className="icon-heart" />
                                    </a>
                                    <a href="#" className="add-to-cart">
                                      <i className="icon-handbag" />
                                    </a>
                                    <a
                                      href="#"
                                      className="quick-view"
                                      data-toggle="modal"
                                      data-target="#exampleModalCenter"
                                    >
                                      <i className="icon-shuffle" />
                                    </a>
                                  </div>
                                </div>
                                <div className="product-content">
                                  <h3>
                                    <Link to="/details">Products Name 009</Link>
                                  </h3>
                                  <div className="price-box">
                                    <span className="new-price">173.00</span>
                                  </div>
                                </div>
                              </div>
                              {/* single-product-wrap end */}
                            </div>
                            <div className="col-lg-3 col-md-4 col-sm-6">
                              {/* single-product-wrap start */}
                              <div className="single-product-wrap">
                                <div className="product-image">
                                  <Link to="/details">
                                    <img
                                      src={require("../assets/images/product/11.jpg")}
                                      alt=""
                                    />
                                  </Link>
                                  <div className="product-action">
                                    <a href="#" className="wishlist">
                                      <i className="icon-heart" />
                                    </a>
                                    <a href="#" className="add-to-cart">
                                      <i className="icon-handbag" />
                                    </a>
                                    <a
                                      href="#"
                                      className="quick-view"
                                      data-toggle="modal"
                                      data-target="#exampleModalCenter"
                                    >
                                      <i className="icon-shuffle" />
                                    </a>
                                  </div>
                                </div>
                                <div className="product-content">
                                  <h3>
                                    <Link to="/details">Products Name 089</Link>
                                  </h3>
                                  <div className="price-box">
                                    <span className="new-price">193.00</span>
                                  </div>
                                </div>
                              </div>
                              {/* single-product-wrap end */}
                            </div>
                          </div>
                        </div>
                      </div>
                      <div id="list" className="tab-pane fade" role="tabpanel">
                        <div className="shop-product-list-wrap">
                          <div className="row product-layout-list">
                            <div className="col-lg-4 col-md-5">
                              {/* single-product-wrap start */}
                              <div className="single-product-wrap">
                                <div className="product-image">
                                  <Link to="/details">
                                    <img
                                      src={require("../assets/images/product/2.jpg")}
                                      alt=""
                                    />
                                  </Link>
                                  <div className="product-action">
                                    <a href="#" className="wishlist">
                                      <i className="icon-heart" />
                                    </a>
                                    <a href="#" className="add-to-cart">
                                      <i className="icon-handbag" />
                                    </a>
                                    <a
                                      href="#"
                                      className="quick-view"
                                      data-toggle="modal"
                                      data-target="#exampleModalCenter"
                                    >
                                      <i className="icon-shuffle" />
                                    </a>
                                  </div>
                                </div>
                              </div>
                              {/* single-product-wrap end */}
                            </div>
                            <div className="col-lg-8 col-md-7">
                              <div className="product-content text-left">
                                <h3>
                                  <Link to="/details">Products Name 012</Link>
                                </h3>
                                <div className="price-box">
                                  <span className="new-price">120.00</span>
                                  <span className="old-price">130.00</span>
                                </div>
                                <p>
                                  Lorem ipsum dolor sit amet, consectetur
                                  adipisicing elit. Veritatis pariatur ipsa sint
                                  consectetur velit maiores sit voluptates aut
                                  tempora totam, consequatur iste quod suscipit
                                  natus. Explicabo facere neque adipisci odio.
                                </p>
                              </div>
                            </div>
                          </div>
                          <div className="row product-layout-list">
                            <div className="col-lg-4 col-md-5">
                              {/* single-product-wrap start */}
                              <div className="single-product-wrap">
                                <div className="product-image">
                                  <Link to="/details">
                                    <img
                                      src={require("../assets/images/product/3.jpg")}
                                      alt=""
                                    />
                                  </Link>
                                  <div className="product-action">
                                    <a href="#" className="wishlist">
                                      <i className="icon-heart" />
                                    </a>
                                    <a href="#" className="add-to-cart">
                                      <i className="icon-handbag" />
                                    </a>
                                    <a
                                      href="#"
                                      className="quick-view"
                                      data-toggle="modal"
                                      data-target="#exampleModalCenter"
                                    >
                                      <i className="icon-shuffle" />
                                    </a>
                                  </div>
                                </div>
                              </div>
                              {/* single-product-wrap end */}
                            </div>
                            <div className="col-lg-8 col-md-7">
                              <div className="product-content text-left">
                                <h3>
                                  <Link to="/details">Products Name 003</Link>
                                </h3>
                                <div className="price-box">
                                  <span className="new-price">121.00</span>
                                  <span className="old-price">132.00</span>
                                </div>
                                <p>
                                  Lorem ipsum dolor sit amet, consectetur
                                  adipisicing elit. Veritatis pariatur ipsa sint
                                  consectetur velit maiores sit voluptates aut
                                  tempora totam, consequatur iste quod suscipit
                                  natus. Explicabo facere neque adipisci odio.
                                </p>
                              </div>
                            </div>
                          </div>
                          <div className="row product-layout-list">
                            <div className="col-lg-4 col-md-5">
                              {/* single-product-wrap start */}
                              <div className="single-product-wrap">
                                <div className="product-image">
                                  <Link to="/details">
                                    <img
                                      src={require("../assets/images/product/4.jpg")}
                                      alt=""
                                    />
                                  </Link>
                                  <div className="product-action">
                                    <a href="#" className="wishlist">
                                      <i className="icon-heart" />
                                    </a>
                                    <a href="#" className="add-to-cart">
                                      <i className="icon-handbag" />
                                    </a>
                                    <a
                                      href="#"
                                      className="quick-view"
                                      data-toggle="modal"
                                      data-target="#exampleModalCenter"
                                    >
                                      <i className="icon-shuffle" />
                                    </a>
                                  </div>
                                </div>
                              </div>
                              {/* single-product-wrap end */}
                            </div>
                            <div className="col-lg-8 col-md-7">
                              <div className="product-content text-left">
                                <h3>
                                  <Link to="/details">Products Name 004</Link>
                                </h3>
                                <div className="price-box">
                                  <span className="new-price">111.00</span>
                                  <span className="old-price">162.00</span>
                                </div>
                                <p>
                                  Lorem ipsum dolor sit amet, consectetur
                                  adipisicing elit. Veritatis pariatur ipsa sint
                                  consectetur velit maiores sit voluptates aut
                                  tempora totam, consequatur iste quod suscipit
                                  natus. Explicabo facere neque adipisci odio.
                                </p>
                              </div>
                            </div>
                          </div>
                          <div className="row product-layout-list">
                            <div className="col-lg-4 col-md-5">
                              {/* single-product-wrap start */}
                              <div className="single-product-wrap">
                                <div className="product-image">
                                  <Link to="/details">
                                    <img
                                      src={require("../assets/images/product/6.jpg")}
                                      alt
                                    />
                                  </Link>
                                  <div className="product-action">
                                    <a href="#" className="wishlist">
                                      <i className="icon-heart" />
                                    </a>
                                    <a href="#" className="add-to-cart">
                                      <i className="icon-handbag" />
                                    </a>
                                    <a
                                      href="#"
                                      className="quick-view"
                                      data-toggle="modal"
                                      data-target="#exampleModalCenter"
                                    >
                                      <i className="icon-shuffle" />
                                    </a>
                                  </div>
                                </div>
                              </div>
                              {/* single-product-wrap end */}
                            </div>
                            <div className="col-lg-8 col-md-7">
                              <div className="product-content text-left">
                                <h3>
                                  <Link to="/details">Products Name 005</Link>
                                </h3>
                                <div className="price-box">
                                  <span className="new-price">89.00</span>
                                  <span className="old-price">99.00</span>
                                </div>
                                <p>
                                  Lorem ipsum dolor sit amet, consectetur
                                  adipisicing elit. Veritatis pariatur ipsa sint
                                  consectetur velit maiores sit voluptates aut
                                  tempora totam, consequatur iste quod suscipit
                                  natus. Explicabo facere neque adipisci odio.
                                </p>
                              </div>
                            </div>
                          </div>
                          <div className="row product-layout-list">
                            <div className="col-lg-4 col-md-5">
                              {/* single-product-wrap start */}
                              <div className="single-product-wrap">
                                <div className="product-image">
                                  <Link to="/details">
                                    <img
                                      src={require("../assets/images/product/8.jpg")}
                                      alt=""
                                    />
                                  </Link>
                                  <div className="product-action">
                                    <a href="#" className="wishlist">
                                      <i className="icon-heart" />
                                    </a>
                                    <a href="#" className="add-to-cart">
                                      <i className="icon-handbag" />
                                    </a>
                                    <a
                                      href="#"
                                      className="quick-view"
                                      data-toggle="modal"
                                      data-target="#exampleModalCenter"
                                    >
                                      <i className="icon-shuffle" />
                                    </a>
                                  </div>
                                </div>
                              </div>
                              {/* single-product-wrap end */}
                            </div>
                            <div className="col-lg-8 col-md-7">
                              <div className="product-content text-left">
                                <h3>
                                  <Link to="/details">Products Name 008</Link>
                                </h3>
                                <div className="price-box">
                                  <span className="new-price">86.00</span>
                                  <span className="old-price">97.00</span>
                                </div>
                                <p>
                                  Lorem ipsum dolor sit amet, consectetur
                                  adipisicing elit. Veritatis pariatur ipsa sint
                                  consectetur velit maiores sit voluptates aut
                                  tempora totam, consequatur iste quod suscipit
                                  natus. Explicabo facere neque adipisci odio.
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* shop-products-wrap end */}
                  {/* paginatoin-area start */}
                  <div className="paginatoin-area">
                    <div className="row">
                      <div className="col-lg-6 col-md-6">
                        <p>Showing 10-13 of 13 item(s) </p>
                      </div>
                      <div className="col-lg-6 col-md-6">
                        <ul className="pagination-box">
                          <li>
                            <a className="Previous" href="#">
                              Previous
                            </a>
                          </li>
                          <li className="active">
                            <a href="#">1</a>
                          </li>
                          <li>
                            <a href="#">2</a>
                          </li>
                          <li>
                            <a href="#">3</a>
                          </li>
                          <li>
                            <a className="Next" href="#">
                              {" "}
                              Next{" "}
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  {/* paginatoin-area end */}
                </div>
                {/* shop-product-wrapper end */}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
