import React, { Component } from "react";

export default class ContactUs extends Component {
  render() {
    return <div><div className="breadcrumb-area section-ptb">
  <div className="container">
    <div className="row">
      <div className="col">
        <h2 className="breadcrumb-title">Contact US</h2>
        {/* breadcrumb-list start */}
        <ul className="breadcrumb-list">
          <li className="breadcrumb-item">
            <a href="index.html">Home</a>
          </li>
          <li className="breadcrumb-item active">Contact US</li>
        </ul>
        {/* breadcrumb-list end */}
      </div>
    </div>
  </div>
</div>
    <div className="main-content-wrap section-ptb contact-us-page">
  <div className="container">
    <div className="row">
      <div className="col-lg-6">
        <div className="contact-info-wrapper">
          <h2>Get in Touch</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum
            earum eveniet dolorum suscipit nesciunt incidunt animi repudiandae
            ab at, tenetur distinctio voluptate vel illo similique.
          </p>
          <ul className="contact-info-list">
            <li>
              {" "}
              <strong>Address:</strong> House 09, Road 32, mohammadpur, Dhaka
              1212
            </li>
            <li>
              <strong>Phone:</strong> +966 11 11 146
            </li>
            <li>
              <strong>Email:</strong> <a href="#"> yoursite@demo.com</a>
            </li>
          </ul>
          <div className="contact-form-warp">
            <form
              id="contact-form"
              action="../../external.html?link=https://demo.hasthemes.com/fusta/mail.php"
              method="post"
            >
              <div className="row">
                <div className="col-lg-10">
                  <input type="text" name="name" placeholder="Your Name*" />
                </div>
                <div className="col-lg-10">
                  <input
                    type="email"
                    name="email"
                    placeholder="Mail Address*"
                  />
                </div>
                <div className="col-lg-10">
                  <textarea
                    name="message"
                    placeholder="Your Massage*"
                    defaultValue={""}
                  />
                </div>
              </div>
              <div className="contact-submit-btn">
                <button type="submit" className="submit-btn">
                  Send Email
                </button>
                <p className="form-messege" />
              </div>
            </form>
          </div>
        </div>
      </div>
      <div className="col-lg-6">
        <div className="google-map-area">
          <div id="map-inner" className="map" />
        </div>
      </div>
    </div>
  </div>
</div>
</div>;
  }
}
