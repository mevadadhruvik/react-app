import React, { Component } from "react";
import { Link } from "react-router-dom";
import logo from "../logo.svg";

export default class Navbar extends Component {
  render() {
    return (
      <div className="header-bottom-area header-sticky">
        <div className="container">
          <div className="row">
            <div className="col-lg-2 col-md-5 col-5">
              <div className="logo">
                <a href="index.html">
                  <img src={require("../assets/images/logo/logo.jpg")} alt="" />
                </a>
              </div>
            </div>
            <div className="col-lg-8 d-none d-lg-block">
              <div className="main-menu-area text-center">
                <nav className="main-navigation" style={{ display: "block" }}>
                  <ul>
                    <li className="active">
                      <Link to="/">Home</Link>
                    </li>
                    <li>
                      <Link to="/shop">Shop</Link>
                    </li>
                    <li>
                      <Link to="/blog">Blog</Link>
                    </li>
                    <li>
                      <Link to="/about">About</Link>
                    </li>
                    <li>
                      <Link to="/contact">Contact us</Link>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
            <div className="col-lg-2 col-md-7 col-7">
              <div className="right-blok-box d-flex">
                <div className="search-wrap">
                  <a href="#" className="trigger-search">
                    <i className="icon-magnifier" />
                  </a>
                </div>
                <div className="user-wrap">
                  <Link to="/login">
                    <i className="icon-user" />
                  </Link>
                </div>
                <div className="shopping-cart-wrap">
                  <Link to="/cart">
                    <i className="icon-handbag" />
                    <span id="cart-total">2</span>
                  </Link>
                  <ul className="mini-cart">
                    <li className="cart-item">
                      <div className="cart-image">
                        <a href="single-product.html">
                          <img alt="" src="assets/images/product/1.jpg" />
                        </a>
                      </div>
                      <div className="cart-title">
                        <a href="single-product.html">
                          <h4>Product Name 01</h4>
                        </a>
                        <span className="quantity">1 ×</span>
                        <div className="price-box">
                          <span className="new-price">$130.00</span>
                        </div>
                        <a className="remove_from_cart" href="#">
                          <i className="icon-trash icons" />
                        </a>
                      </div>
                    </li>
                    <li className="cart-item">
                      <div className="cart-image">
                        <a href="single-product.html">
                          <img alt="" src="assets/images/product/3.jpg" />
                        </a>
                      </div>
                      <div className="cart-title">
                        <a href="single-product.html">
                          <h4>Product Name 03</h4>
                        </a>
                        <span className="quantity">1 ×</span>
                        <div className="price-box">
                          <span className="new-price">$130.00</span>
                        </div>
                        <a className="remove_from_cart" href="#">
                          <i className="icon-trash icons" />
                        </a>
                      </div>
                    </li>
                    <li className="subtotal-titles">
                      <div className="subtotal-titles">
                        <h3>Sub-Total :</h3>
                        <span>$ 230.99</span>
                      </div>
                    </li>
                    <li className="mini-cart-btns">
                      <div className="cart-btns">
                        <a href="cart.html">View cart</a>
                        <a href="checkout.html">Checkout</a>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col">
              <div className="mobile-menu d-block d-lg-none" />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
