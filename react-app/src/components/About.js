import React, { Component } from "react";

class About extends Component {
  render() {
    return (
      <div>
        <div className="breadcrumb-area section-ptb">
          <div className="container">
            <div className="row">
              <div className="col">
                <h2 className="breadcrumb-title">About Us</h2>
                {/* breadcrumb-list start */}
                <ul className="breadcrumb-list">
                  <li className="breadcrumb-item">
                    <a href="index.html">Home</a>
                  </li>
                  <li className="breadcrumb-item active">About Us</li>
                </ul>
                {/* breadcrumb-list end */}
              </div>
            </div>
          </div>
        </div>
        <br /> <br /> <br /> <br />
        <div className="project-count-area pb--70">
          <div className="container">
            <div className="row">
              <div className="col-lg-3 col-md-3 col-sm-6">
                <div className="single-count text-center mb--30">
                  <div className="count-title">
                    <h2 className="count">21</h2>
                    <span>Years in Business</span>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-3 col-sm-6">
                <div className="single-count text-center mb--30">
                  <div className="count-title">
                    <h2 className="count">210</h2>
                    <span>Clients and Partners</span>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-3 col-sm-6">
                <div className="single-count text-center mb--30">
                  <div className="count-title">
                    <h2 className="count">18</h2>
                    <span>Show Room</span>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-3 col-sm-6">
                <div className="single-count text-center mb--30">
                  <div className="count-title">
                    <h2 className="count">17</h2>
                    <span>Billon Sales</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default About;
