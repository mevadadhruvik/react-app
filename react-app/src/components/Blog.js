import React, { Component } from "react";

class Blog extends Component {
  render() {
    return (
      <div>
        <div>
          <div className="breadcrumb-area section-ptb">
            <div className="container">
              <div className="row">
                <div className="col">
                  <h2 className="breadcrumb-title">Blog Full Width</h2>
                  {/* breadcrumb-list start */}
                  <ul className="breadcrumb-list">
                    <li className="breadcrumb-item">
                      <a href="index.html">Home</a>
                    </li>
                    <li className="breadcrumb-item active">Blog Full Width</li>
                  </ul>
                  {/* breadcrumb-list end */}
                </div>
              </div>
            </div>
          </div>
          {/* breadcrumb-area end */}
          {/* main-content-wrap start */}
          <div className="main-content-wrap blog-page section-ptb">
            <div className="container">
              <div className="row">
                <div className="col-lg-8 offset-lg-2">
                  {/* blog-wrapper start */}
                  <div className="blog-wrapper">
                    {/* blog-wrap start */}
                    <div className="blog-wrap">
                      {/* single-blog-area start */}
                      <div className="single-blog-area">
                        <div className="blog-image">
                          <a href="blog-details.html">
                            <img
                              src={require("../assets/images/product/2.jpg")}
                              alt
                            />
                          </a>
                        </div>
                        <div className="blog-contend">
                          <h3>
                            <a href="#">Blog image post</a>
                          </h3>
                          <div className="blog-date-categori">
                            <ul>
                              <li>
                                <a href="#">
                                  <i className="ion-ios-person" /> Admin{" "}
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <i className="ion-ios-pricetags" /> Comments{" "}
                                </a>
                              </li>
                            </ul>
                          </div>
                          <p>
                            Lorem ipsum dolor sit amet, consectetur adip elit,
                            sed do eiusmod tempor incididunt labore et dolore
                            magna aliqua. Ut enim ad minim veniam, quis nostrud
                            exercit ullamco laboris nisi ut aliquip ex ea
                            commodo consequat. Duis aute irure dolor in
                            reprehenderit voluptate velit esse cillum dolore eu
                            fugiat nulla pariatur.{" "}
                          </p>
                          <div className="mt-30 blog-more-area">
                            <a
                              href="blog-details.html"
                              className="blog-btn btn"
                            >
                              Read more
                            </a>
                            <ul className="social-icons">
                              <li>
                                <a href="#">
                                  <i className="ion-social-facebook" />
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <i className="ion-social-twitter" />
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <i className="ion-social-dribbble" />
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <i className="ion-social-googleplus" />
                                </a>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      {/* single-blog-area end */}
                      {/* single-blog-area start */}
                      <div className="single-blog-area">
                        <div className="blog-image-slider">
                          <a href="blog-details.html">
                            <img
                              src={require("../assets/images/blog/3.jpg")}
                              alt
                            />
                          </a>
                          <a href="blog-details.html">
                            <img
                              src={require("../assets/images/blog/1.jpg")}
                              alt
                            />
                          </a>
                          <a href="blog-details.html">
                            <img
                              src={require("../assets/images/blog/4.jpg")}
                              alt
                            />
                          </a>
                        </div>
                        <div className="blog-contend">
                          <h3>
                            <a href="#">Post with Gallery </a>
                          </h3>
                          <div className="blog-date-category">
                            <ul>
                              <li>
                                <a href="#">
                                  <i className="ion-ios-person" /> Admin{" "}
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <i className="ion-ios-pricetags" /> Comments{" "}
                                </a>
                              </li>
                            </ul>
                          </div>
                          <p>
                            Lorem ipsum dolor sit amet, consectetur adip elit,
                            sed do eiusmod tempor incididunt labore et dolore
                            magna aliqua. Ut enim ad minim veniam, quis nostrud
                            exercit ullamco laboris nisi ut aliquip ex ea
                            commodo consequat. Duis aute irure dolor in
                            reprehenderit voluptate velit esse cillum dolore eu
                            fugiat nulla pariatur.{" "}
                          </p>
                          <div className="mt-30 blog-more-area">
                            <a
                              href="blog-details.html"
                              className="blog-btn btn"
                            >
                              Read more
                            </a>
                            <ul className="social-icons">
                              <li>
                                <a href="#">
                                  <i className="ion-social-facebook" />
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <i className="ion-social-twitter" />
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <i className="ion-social-dribbble" />
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <i className="ion-social-googleplus" />
                                </a>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      {/* single-blog-area end */}
                      {/* single-blog-area start */}
                      <div className="single-blog-area">
                        <div className="blog-image">
                          <div className="blog-audio embed-responsive embed-responsive-16by9">
                            <iframe src="../../external.html?link=https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/244702956&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&visual=true" />
                          </div>
                        </div>
                        <div className="blog-contend">
                          <h3>
                            <a href="#">Post With Audio</a>
                          </h3>
                          <div className="blog-date-categori">
                            <ul>
                              <li>
                                <a href="#">
                                  <i className="ion-ios-person" /> Admin{" "}
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <i className="ion-ios-pricetags" /> Comments{" "}
                                </a>
                              </li>
                            </ul>
                          </div>
                          <p>
                            Lorem ipsum dolor sit amet, consectetur adip elit,
                            sed do eiusmod tempor incididunt labore et dolore
                            magna aliqua. Ut enim ad minim veniam, quis nostrud
                            exercit ullamco laboris nisi ut aliquip ex ea
                            commodo consequat. Duis aute irure dolor in
                            reprehenderit voluptate velit esse cillum dolore eu
                            fugiat nulla pariatur.{" "}
                          </p>
                          <div className="mt-30 blog-more-area">
                            <a
                              href="blog-details.html"
                              className="blog-btn btn"
                            >
                              Read more
                            </a>
                            <ul className="social-icons">
                              <li>
                                <a href="#">
                                  <i className="ion-social-facebook" />
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <i className="ion-social-twitter" />
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <i className="ion-social-dribbble" />
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <i className="ion-social-googleplus" />
                                </a>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      {/* single-blog-area end */}
                      {/* single-blog-area start */}
                      <div className="single-blog-area">
                        <div className="blog-image">
                          <div className="embed-responsive embed-responsive-16by9">
                            <iframe
                              allowFullScreen="true"
                              src="../../external.html?link=https://www.youtube.com/embed/5T-_dYz8Uvw"
                              width={600}
                              height={400}
                              frameBorder={0}
                            />
                          </div>
                        </div>
                        <div className="blog-contend">
                          <h3>
                            <a href="#">Post with Video</a>
                          </h3>
                          <div className="blog-date-categori">
                            <ul>
                              <li>
                                <a href="#">
                                  <i className="ion-ios-person" /> Admin{" "}
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <i className="ion-ios-pricetags" /> Comments{" "}
                                </a>
                              </li>
                            </ul>
                          </div>
                          <p>
                            Lorem ipsum dolor sit amet, consectetur adip elit,
                            sed do eiusmod tempor incididunt labore et dolore
                            magna aliqua. Ut enim ad minim veniam, quis nostrud
                            exercit ullamco laboris nisi ut aliquip ex ea
                            commodo consequat. Duis aute irure dolor in
                            reprehenderit voluptate velit esse cillum dolore eu
                            fugiat nulla pariatur.{" "}
                          </p>
                          <div className="mt-30 blog-more-area">
                            <a
                              href="blog-details.html"
                              className="blog-btn btn"
                            >
                              Read more
                            </a>
                            <ul className="social-icons">
                              <li>
                                <a href="#">
                                  <i className="ion-social-facebook" />
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <i className="ion-social-twitter" />
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <i className="ion-social-dribbble" />
                                </a>
                              </li>
                              <li>
                                <a href="#">
                                  <i className="ion-social-googleplus" />
                                </a>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      {/* single-blog-area end */}
                    </div>
                    {/* blog-wrap end */}
                    {/* paginatoin-area start */}
                    <div className="paginatoin-area">
                      <div className="row">
                        <div className="col-lg-6 col-md-6">
                          <p>Showing 10-13 of 13 item(s) </p>
                        </div>
                        <div className="col-lg-6 col-md-6">
                          <ul className="pagination-box">
                            <li>
                              <a className="Previous" href="#">
                                Previous
                              </a>
                            </li>
                            <li className="active">
                              <a href="#">1</a>
                            </li>
                            <li>
                              <a href="#">2</a>
                            </li>
                            <li>
                              <a href="#">3</a>
                            </li>
                            <li>
                              <a className="Next" href="#">
                                {" "}
                                Next{" "}
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    {/* paginatoin-area end */}
                  </div>
                  {/* blog-wrapper end */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Blog;
