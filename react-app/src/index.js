import React from "react";
import ReactDOM from "react-dom";
// import "./index.css";
import "./assets/css/bootstrap.min.css";
import "./assets/css/ionicons.min.css";
import "./assets/css/plugins.css";
import "./assets/css/simple-line-icons.css";
import "./assets/css/style.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { BrowserRouter as Router, Route } from "react-router-dom";

ReactDOM.render(
  <Router>
    <App />
  </Router>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
